﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.Interfaces;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Integrity
{
    public class DiseaseIntegrity
    {
        private ILogger _logger;

        public DiseaseIntegrity(ILogger<DiseaseIntegrity> logger)
        {
            this._logger = logger;
        }

        private void ValidateCommon(Disease disease, IBaseResponse response)
        {
            if (string.IsNullOrWhiteSpace(disease.Name))
            {
                response.AddValidationMessage("000", "Doença não pode ter nome vazio");
            }

            if (disease.TransmissionRate > 100)
            {
                response.AddValidationMessage("000", "Doença não pode ter taxa de transmissibilidade superior a 100%");
            }
            else if (disease.TransmissionRate < 0)
            {
                response.AddValidationMessage("000", "Doença não pode ter taxa de transmissibilidade menor que 0%");
            }

            if (disease.DeathAverage > 100)
            {
                response.AddValidationMessage("000", "Doença não pode ter taxa de letalidade superior a 100%");
            }
            else if (disease.DeathAverage < 0)
            {
                response.AddValidationMessage("000", "Doença não pode ter taxa de letalidade menor que 0%");
            }
        }
        public void ValidateInsert(Disease disease, IBaseResponse response)
        {
            ValidateCommon(disease, response);
        }
        public void ValidateUpdate(Disease disease, IBaseResponse response)
        {
            if (disease.Id == 0)
            {
                response.AddValidationMessage("000", "Esta Doença não pode ser atualizada");
            }
            ValidateCommon(disease, response);
        }
    }
}
