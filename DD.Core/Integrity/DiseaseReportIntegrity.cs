﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Integrity
{
    public class DiseaseReportIntegrity
    {
        ILogger _logger;

        public DiseaseReportIntegrity(ILogger<DiseaseReportIntegrity> logger)
        {
            this._logger = logger;
        }

        private void ValidateCommon(DiseaseReport report, IBaseResponse response)
        {
            if (string.IsNullOrWhiteSpace(report.InfectedPerson.Name))
            {
                response.AddValidationMessage("000", "Nome do paciente infectado não pode ser vazio");
            }

            if (string.IsNullOrWhiteSpace(report.InfectedPerson.UniqueDocument))
            {
                response.AddValidationMessage("000", "CPF do paciente infectado não pode ser vazio");
            }

            if (report.Disease.Id == 0)
            {
                response.AddValidationMessage("000", "Selecione uma doença válida para salvar o reporte");
            }

            if (!report.ListStatus.Any() || report.ListStatus.Exists(x => x.Id == 0))
            {
                response.AddValidationMessage("000", "Selecione a severidade da infecção");
            }

            if (report.InfectedPerson.Address.Id_Neighborhood == 0)
            {
                response.AddValidationMessage("000", "Selecione o bairro em que vive o paciente");
            }

        }

        public void ValidateInsert(DiseaseReport report, IBaseResponse response)
        {
            ValidateCommon(report, response);
        }

        public void ValidateUpdate(DiseaseReport report, IBaseResponse response)
        {
            if(report.Id == 0)
            {
                response.AddValidationMessage("000", "Esse report não pode ser atualizado");
            }

            ValidateCommon(report, response);
        }
    }
}
