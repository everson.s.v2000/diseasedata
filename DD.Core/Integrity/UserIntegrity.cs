﻿using DD.Core.Models;
using DD.Core.Models.DTO;
using Microsoft.Extensions.Logging;
using Nedesk.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Integrity
{
    public class UserIntegrity
    {
        private readonly ILogger _logger;

        public UserIntegrity(ILogger<UserIntegrity> logger)
        {
            this._logger = logger;
        }

        private void ValidateCommon(RegisterUser user, IBaseResponse response)
        {
            if (string.IsNullOrWhiteSpace(user.Name))
            {
                response.AddValidationMessage("000", "Nome não pode ser vazio");
            }

            if (string.IsNullOrWhiteSpace(user.Password))
            {
                response.AddValidationMessage("000", "Senha não pode ser vazia");
            }
            else if (user.Password.Length < 6)
            {
                response.AddValidationMessage("000", "Informeu ma senha com mais de 6 dígitos");
            }

            if (string.IsNullOrWhiteSpace(user.Email))
            {
                response.AddValidationMessage("000", "E-mail não pode estar em branco");
            }

            if (!user.Email.Contains("@"))
            {
                response.AddValidationMessage("000", "E-mail inválido, informe um e-mail válido");
            }

            if (string.IsNullOrWhiteSpace(user.Address.Street))
            {
                response.AddValidationMessage("000", "Informe a rua");
            }

            if (user.Address.Number == 0)
                    {
                response.AddValidationMessage("000", "Informe o número da casa ou apartamento.");
            }

            if (user.Address.Id_Neighborhood == 0)
            {
                response.AddValidationMessage("000", "Selecione um bairro válido");
            }
        }

        public void ValidateInsert(DTO_RegisterUser user, IBaseResponse response)
        {
            ValidateCommon(user,response);

            if (user.ConfirmPassword != user.Password)
            {
                response.AddValidationMessage("000", "Senha e confirmação de senha devem ser a mesma senha");
            }

        }

        public void ValidateUpdate(DTO_UpdateUser user, IBaseResponse response)
        {
            ValidateCommon(user, response);

            if (user.Id <= 0)
            {
                response.AddValidationMessage("000", "Não é possível atualizar esse usuário");
            }

            if (user.OldPassword == user.Password)
            {
                response.AddValidationMessage("000", "Nova senha não pode ser igual a senha anterior");
            }

        }
    }
}
