﻿using DD.Core.Business;
using DD.Core.Models;
using Microsoft.AspNetCore.Http;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.ApiCore
{
    public class DDAuth
    {
        private LoginBusiness _loginBusiness;
        private UserBusiness _userBusiness;
        private IHttpContextAccessor _httpContextAccessor;
        private RegisterUser _currentUser;

        
        public DDAuth(LoginBusiness loginBusiness,
                              UserBusiness userBusines, 
                              IHttpContextAccessor httpContextAccessor)
        {
            this._loginBusiness = loginBusiness;
            this._userBusiness = userBusines;
            this._httpContextAccessor = httpContextAccessor;
        }

        public void IsAuthenticated<T>(BaseResponse<T> response)
        {
            string currentSession = _httpContextAccessor.HttpContext.Request.Headers["Session"];


            response.Merge(_loginBusiness.ValidateSession(currentSession));
        }

        public void IsAuthorized<T>(BaseResponse<T> response,List<Role> roles)
        {
            var userResponse = _userBusiness.FindById(0);

            this._currentUser = userResponse.ResponseData;

            foreach(Role role in roles)
            {
                if(!this._currentUser.Roles.Exists(x => x.Id == role.Id))
                {
                    response.AddValidationMessage("0", "Usuário não possui autorização para essa ação.");
                    return;
                }
            }
        }
    }
}
