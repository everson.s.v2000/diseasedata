﻿using DD.Core.Business;
using DD.Core.Cache;
using DD.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.ApiCore
{
    public class DDControllerBase : ControllerBase
    {
        protected readonly LoginBusiness _loginBusiness;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        private DDAuth _DDAuthenticate;

        public DDControllerBase(LoginBusiness loginBusiness,
                                DDAuth ddAuthenticate,
                                IHttpContextAccessor httpContextAccessor
                                )
        {
            this._loginBusiness = loginBusiness;
            this._httpContextAccessor = httpContextAccessor;
            this._DDAuthenticate = ddAuthenticate;
        }

        
        
        protected ActionResult<Response<T>> GetResponse<T>(Func<Response<T>> func, List<Role> roles = null, bool requireAuthentication = true)
        {
            Response<T> response = new();
            if(requireAuthentication)
                _DDAuthenticate.IsAuthenticated(response);

            if (roles != null)
                _DDAuthenticate.IsAuthorized(response, roles);

            if (response.HasValidationMessages)
                return response;


            Task<Response<T>> task = new Task<Response<T>>(func);
            
            task.RunSynchronously();
            response = task.Result;
           
            return response.StatusCode switch
            {
                HttpStatusCode.OK => Ok(response),
                HttpStatusCode.BadRequest => BadRequest(response),
                _ => Ok(response)
            }; 
        }
        protected ActionResult<InquiryResponse<T>> GetInquiryResponse<T>(Func<InquiryResponse<T>> func, List<Role> roles = null, bool requireAuthentication = true)
        {
            InquiryResponse<T> response = new();
            if(requireAuthentication)
                _DDAuthenticate.IsAuthenticated(response);

            if (roles != null)
                _DDAuthenticate.IsAuthorized(response, roles);

            if (response.HasValidationMessages)
                return response;


            Task<InquiryResponse<T>> task = new Task<InquiryResponse<T>>(func);

            task.RunSynchronously();
            response = task.Result;

            return response.StatusCode switch
            {
                HttpStatusCode.OK => Ok(response),
                HttpStatusCode.BadRequest => BadRequest(response),
                _ => Ok(response)
            };
        }
        protected ActionResult<Response<T>> GetErrorResponse<T>()
        {

            return BadRequest(new Response<T>
            {
                Messages = new()
                {
                    {
                        new()
                        {
                           Code = "900",
                           Text = "Something went wrong",
                           MessageType = Nedesk.Core.Enums.MessageTypeEnum.FatalError
                        }
                    }
                }
            });
        }
        protected ActionResult<InquiryResponse<T>> GetErrorInquiryResponse<T>()
        {
            return BadRequest(new InquiryResponse<T>
            {
                Messages = new()
                {
                    {
                        new()
                        {
                            Code = "900",
                            Text = "Something went wrong",
                            MessageType = Nedesk.Core.Enums.MessageTypeEnum.FatalError
                        }
                    }
                }
            });
        }
    }
}
