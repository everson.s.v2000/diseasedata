﻿using DD.Core.Models;
using DD.Core.Repository;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Cache
{
    public class RoleCache : ICached
    {
        private List<Role> _roles;
        private ILogger _logger;
        private IRepository _repository;
        private RoleRepository _roleRepository;

        public IReadOnlyCollection<Role> Roles
        {
            get
            {
                return _roles;
            }
        }

        public RoleCache(ILogger<RoleCache> logger, IRepository repository, ILoggerFactory loggerFactory)
        {
            this._logger = logger;
            this._repository = repository;
            this._roleRepository = new RoleRepository(repository, new Logger<RoleRepository>(loggerFactory));
            FetchData();
        }

        public async Task UpdateCacheAsync()
        {
            _ = Task.Run(async () => FetchData());
        }

        private void FetchData()
        {
            try
            {
                InquiryResponse<Role> response = this._roleRepository.FindAll();
                if (response.InError)
                {
                    _logger.LogError("Error when attempting to fetch all Roles.");
                    if (_roles == null)
                    {
                        _roles = new();
                        return;
                    }

                    _roles.Clear();
                    return;
                }

                _roles = response.ResponseData;
            }
            catch
            {
                throw;
            }
            finally
            {
                _repository.CloseConnection();
            }
        }
    }
}
