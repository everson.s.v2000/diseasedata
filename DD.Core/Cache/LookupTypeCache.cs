﻿using DD.Core.Models;
using DD.Core.Repository;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using System;
using System.Collections.Generic;
using DD.Core.Enums;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Cache
{
    public class LookupTypeCache : ICached
    {
        private ILogger _logger;
        private IRepository _repository;
        private LookupTypeRepository _lookupTypeRepository;

        #region Datasource

        private List<LookupType> _documentTypes = new();
        private List<LookupType> _infectionServerityTypes = new();
        private List<LookupType> _pathogenTypes = new();
        private List<LookupType> _transmissionType = new();
        public IReadOnlyCollection<LookupType> DocumentTypes
        {
            get
            {
                return _documentTypes;
            }
        }
        public IReadOnlyCollection<LookupType> InfectionServerityTypes
        {
            get
            {
                return _infectionServerityTypes;
            }
        }
        public IReadOnlyCollection<LookupType> PathogenTypes
        {
            get
            {
                return _pathogenTypes;
            }
        }
        public IReadOnlyCollection<LookupType> TransmissionTypes
        {
            get
            {
                return _transmissionType;
            }
        }

        #endregion
        public async Task UpdateCacheAsync()
        {
            _ = Task.Run(async () => FetchData());
        }
        public LookupTypeCache(ILogger<LookupTypeCache> logger, IRepository repository, ILoggerFactory loggerFactory)
        {
            this._logger = logger;
            this._repository = repository;
            this._lookupTypeRepository = new LookupTypeRepository(repository, new Logger<LookupTypeRepository>(loggerFactory));
            FetchData();
        }

        private void FetchData()
        {
            try
            {
                FetchData(LookupEnum.DocumentType, _documentTypes);
                FetchData(LookupEnum.InfectionSeverityType, _infectionServerityTypes);
                FetchData(LookupEnum.PathogenType, _pathogenTypes);
                FetchData(LookupEnum.TransmissionType, _transmissionType);
            }
            catch
            {
                throw;
            }
            finally
            {
                _repository.CloseConnection();
            }
        }
        private void FetchData(LookupEnum targetType, List<LookupType> dataSource)
        {
            try
            {
                this._logger.LogInformation($"Attempting to Fetch the lookup:{targetType}");
                InquiryResponse<LookupType> response;
                response = _lookupTypeRepository.FindAll(targetType);
                if (response.InError)
                    throw new Exception($"Failed to retrieve '{targetType}s' from DataBase");

                dataSource = response.ResponseData;

            }
            catch
            {
                throw;
            }
        }
    }
}
