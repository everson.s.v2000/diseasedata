﻿using Nedesk.Core.Models;
using System;
using System.Collections.Generic;

namespace DD.Core.Models
{
    public class InquiryResponse<T> : Response<List<T>>
    {
        public int PageIndex { get; set; }
        public int ItemsPerPage { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages
        {
            get
            {
                if (ItemsPerPage == 0)
                    return 1;

                double value = (double)TotalItems / (double)ItemsPerPage;
                if(value != (int)value)
                {
                   value =  Math.Round(value, 0, MidpointRounding.ToPositiveInfinity);
                }

                if (value == 0) return 1;

                return (int)value;
            }
        }

        public void MergeFromRequest(ListRequest request)
        {
            this.PageIndex = request.PageIndex;
            this.ItemsPerPage = request.Limit;
        }
    }
}
