﻿using Nedesk.Core.Models;
using System.Collections.Generic;

namespace DD.Core.Models
{
    public class Pathogen : BaseModel
    {
        public string Name { get; set; }
        public List<LookupType> TransmissionTypes { get; set; }
        public LookupType Type { get; set; }
    }
}
