﻿using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Models
{
    public class Disease : BaseModel
    {
        public Pathogen DiseasePathogen { get; set; }
        public List<Symptom> Symptoms { get; set; }
        public string Name { get; set; }
        public string CycleOfLife { get; set; }
        public string Treatment { get; set; }
        public double TransmissionRate { get; set; }
        public double DeathAverage { get; set; }
    }
}
