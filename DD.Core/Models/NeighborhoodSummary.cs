﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Models
{
    public class NeighborhoodSummary
    {
        public string NeighborhoodName { get; set; }
        public int NumberOfCases { get; set; }
        public double VeryHighInfectionsSeverityCount { get; set; }
        public double HighInfectionsSeverityCount { get; set; }
        public double NormalInfectionsSeverityCount { get; set; }
        public double LowInfectionsSeverityCount { get; set; }
        public double VeryLowInfectionsSeverityCount { get; set; }
    }
}
