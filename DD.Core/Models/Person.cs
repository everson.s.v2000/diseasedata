﻿using Nedesk.Core.Models;

namespace DD.Core.Models
{
    public class Person : BaseModel
    {
        public string Name { get; set; }
        public char Sex { get; set; }
        public int Age { get; set; }
        public LookupType DocumentType { get; set; }
        public string UniqueDocument { get; set; }
        public Address Address { get; set; }

    }
}
