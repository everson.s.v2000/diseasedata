﻿using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Models
{
    public class RegisterUser:BaseUser
    {
        public string Phone { get; set; }
        public string Cellphone { get; set; }
        public string Cellphone_2 { get; set; }
        public Address Address { get; set; }

    }
}
