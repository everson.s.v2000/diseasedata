﻿using Nedesk.Core.Models;

namespace DD.Core.Models
{
    public class DiseaseReportStatus:BaseModel
    {
        public int Id_DiseaseReport { get; set; }
        public LookupType InfectionSeverityType { get; set; }
    }
}
