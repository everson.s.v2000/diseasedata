﻿using Nedesk.Core.Models;

namespace DD.Core.Models
{
    public class LookupType : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string AdditionalInfo { get; set; }
    }
}
