﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Models.DTO
{
    public class DTO_UpdateUser:RegisterUser
    {
        public string OldPassword { get; set; }
    }
}
