﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Models.DTO
{
    public class DTO_RegisterUser:RegisterUser
    {
        public string ConfirmPassword { get; set; }
    }
}
