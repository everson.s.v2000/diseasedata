﻿using Nedesk.Core.Models;
using System.Collections.Generic;

namespace DD.Core.Models
{
    public class Country : BaseModel
    {
        public int Id_Continent { get; set; }
        public string Name { get; set; }
        public List<State> States { get; set; }
    }


}
