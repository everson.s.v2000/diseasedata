﻿using Nedesk.Core.Models;
using System.Collections.Generic;

namespace DD.Core.Models
{
    public class Continent:BaseModel
    {
        public string Name { get; set; }
        public List<Country> Countries{ get; set; }
    }
}
