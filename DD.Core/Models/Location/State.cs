﻿using Nedesk.Core.Models;
using System.Collections.Generic;

namespace DD.Core.Models
{
    public class State : BaseModel
    {
        public int Id_Country { get; set; }
        public string Name { get; set; }
        public List<City> Cities { get; set; }
    }


}
