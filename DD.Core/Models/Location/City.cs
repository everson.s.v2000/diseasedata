﻿using Nedesk.Core.Models;
using System.Collections.Generic;

namespace DD.Core.Models
{
    public class City : BaseModel
    {
        public int Id_State { get; set; }
        public string Name { get; set; }
        public List<Neighborhood> Neighborhoods { get; set; }
    }


}
