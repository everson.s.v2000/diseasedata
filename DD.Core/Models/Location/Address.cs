﻿using Nedesk.Core.Models;
using System;

namespace DD.Core.Models
{
    public class Address : BaseModel
    {
        public int Id_Neighborhood { get; set; }
        public int Number { get; set; }
        public string Street { get; set; }
        public string Complement { get; set; }
        public string Reference { get; set; }
    }


}
