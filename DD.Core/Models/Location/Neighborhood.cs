﻿using Nedesk.Core.Models;
using System.Collections.Generic;

namespace DD.Core.Models
{
    public class Neighborhood : BaseModel
    {
        public int Id_City { get; set; }
        public string Name { get; set; }
        public List<Address> Addresses { get; set; }
    }


}
