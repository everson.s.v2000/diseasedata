﻿using Nedesk.Core.Models;
using System.Collections.Generic;

namespace DD.Core.Models
{
    public class DiseaseReport : BaseModel
    {
        public Disease Disease { get; set; }
        public Person InfectedPerson { get; set; }
        public List<DiseaseReportStatus> ListStatus { get; set; }
    }
}
