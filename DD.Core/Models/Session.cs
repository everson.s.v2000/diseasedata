﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Models
{
    public class Session
    {
        public long Id { get; set; }
        public long Id_User { get; set; }
        public string SessionKey { get; set; }
        public DateTime LastUse { get; set; }
    }
}
