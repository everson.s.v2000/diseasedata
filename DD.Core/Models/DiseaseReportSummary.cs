﻿using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Models
{
    public class DiseaseReportSummary: BaseModel
    {
        public string DiseaseName { get; set; }
        public int NumberOfCases { get; set; }
        public double VeryHighInfectionsSeverityCount { get; set; }
        public double HighInfectionsSeverityCount { get; set; }
        public double NormalInfectionsSeverityCount { get; set; }
        public double LowInfectionsSeverityCount { get; set; }
        public double VeryLowInfectionsSeverityCount { get; set; }
        public double DeathAverage { get; set; }
        public double TransmissionRate { get; set; }
        public List<NeighborhoodSummary> NeighborhoodSummaries { get; set; }
    }
}
