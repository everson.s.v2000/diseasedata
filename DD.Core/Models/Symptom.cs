﻿using Nedesk.Core.Models;

namespace DD.Core.Models
{
    public class Symptom : BaseModel
    {
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
