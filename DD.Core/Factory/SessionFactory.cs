﻿using DD.Core.Models;
using Microsoft.Extensions.Configuration;
using Nedesk.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Factory
{
    public  class SessionFactory
    {
        private Encrypter _encrypter;
        private readonly string privateKey;
        private readonly string publicKey;

        public SessionFactory(Encrypter encrypter, IConfiguration configuration)
        {
            _encrypter = encrypter;
            this.publicKey = configuration.GetSection("Keys")["Public"];
            this.privateKey = configuration.GetSection("Keys")["Private"];
        }

        public string CreateSessionByUser(RegisterUser user)
        {
            string session = $"{user.Id}-{user.Email}-{DateTime.UtcNow}";

            return _encrypter.GetMD5String(session);
        }
    }
}
