﻿using DD.Core.Integrity;
using DD.Core.Models;
using DD.Core.Repository;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Business
{
    public class DiseaseBusiness
    {
        private ILogger _logger;
        private IRepository _repository;
        private DiseaseRepository _diseaseRepository;
        private Disease_SymptomsRepository _diseaseSymptomsRepository;
        private DiseaseIntegrity _diseaseIntegrity;

        public DiseaseBusiness(IRepository repository, 
                               ILogger<DiseaseBusiness> logger,
                               DiseaseIntegrity diseaseIntegrity,
                               ILoggerFactory loggerFactory)
        {
            this._repository = repository;
            this._logger = logger;
            this._diseaseIntegrity = diseaseIntegrity;
            this._diseaseRepository = new(repository, new Logger<DiseaseRepository>(loggerFactory));
            this._diseaseSymptomsRepository = new(repository, new Logger<Disease_SymptomsRepository>(loggerFactory));
        }

        public Response<int> Insert(Disease disease)
        {
            Response<int> response = new();
            
            _diseaseIntegrity.ValidateInsert(disease, response);
            if (response.HasValidationMessages)
                return response;

            try
            {

                _repository.BeginTransaction();

                response = _diseaseRepository.Insert(disease);
                if (response.InError)
                {
                    return response;
                }

                disease.Id = response.ResponseData;

                Response<int> disease_symptomResponse = _diseaseSymptomsRepository.Insert(disease);
                
                response.Merge(disease_symptomResponse);
                response.AddInformationMessage("000", "Doença cadastrada com sucesso!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<bool> Update(Disease disease)
        {
            Response<bool> response = new();

            _diseaseIntegrity.ValidateUpdate(disease, response);
            if (response.HasValidationMessages)
                return response;
            try
            {
                _repository.BeginTransaction();

                Response<bool> diseaseResponse = _diseaseRepository.Update(disease);
                if (diseaseResponse.InError)
                {
                    response.Merge(diseaseResponse);
                    return response;
                }
                response.AddInformationMessage("000", "Doença atualizada com sucesso!");

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<Disease> FindById(int id)
        {
            Response<Disease> response = new();
            try
            {
                response = _diseaseRepository.FindById(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("801", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }

        public InquiryResponse<Disease> FetchAll(ListRequest request)
        {
            InquiryResponse<Disease> response = new();
            try
            {
                if (request.Limit == 0 || request.PageIndex == 0)
                    AddDefaultRequestItems(request);

                response = _diseaseRepository.FetchAll(request);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("801", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }

        private void AddDefaultRequestItems(ListRequest request)
        {
            request.Limit = 10;
            request.PageIndex = 1;
            
        }
    }
}
