﻿using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Business
{
    public class RoleBusiness
    {
        private ILogger _logger;
        private IRepository _repository;

        public RoleBusiness(IRepository repository,
                            ILogger<RoleBusiness> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }
    }
}
