﻿using DD.Core.Factory;
using DD.Core.Models;
using DD.Core.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Business
{
    public class LoginBusiness
    {
        private IRepository _repository;
        private ILogger _logger;
        private UserRepository _userRepository;
        private SessionRepository _sessionRepository;
        private Encrypter _encrpyter;
        private SessionFactory _sessionFactory;
        private IHttpContextAccessor _httpAccessor;

        private readonly string privateKey;
        private readonly string publicKey;
        public LoginBusiness(IRepository repository,
                             ILogger<LoginBusiness> logger,
                             ILoggerFactory loggerFactory,
                             IConfiguration configuration,
                             Encrypter encrypter,
                             SessionFactory sessionFactory,
                             IHttpContextAccessor httpAccessor)
        {
            this._repository = repository;
            this._logger = logger;
            this._encrpyter = encrypter;
            this._sessionFactory = sessionFactory;
            this._userRepository = new(repository, new Logger<UserRepository>(loggerFactory));
            this._sessionRepository = new(repository, new Logger<SessionRepository>(loggerFactory));
            this._httpAccessor = httpAccessor;
            this.publicKey = configuration.GetSection("Keys")["Public"];
            this.privateKey = configuration.GetSection("Keys")["Private"];

        }

        public Response<string> Login(RegisterUser user)
        {
            Response<string> response = new();

            try
            {
                Response<RegisterUser> fetchUserResponse = _userRepository.FindByLogin(user.Email,
                                                                       _encrpyter.GetEncrpytString(user.Password, publicKey, privateKey));

                if (fetchUserResponse.InError)
                {
                    response.Merge(fetchUserResponse);
                    return response;
                }

                if (fetchUserResponse.ResponseData == null)
                {
                    response.AddErrorMessage("800", "Email e/ou senha incorretos. Tente novamente.");
                    response.Merge(fetchUserResponse);
                    return response;
                }

                Session session = new Session()
                {
                    Id_User = fetchUserResponse.ResponseData.Id,
                    SessionKey = _sessionFactory.CreateSessionByUser(user),
                    LastUse = DateTime.UtcNow,
                };

                Response<int> insertSessionResponse = _sessionRepository.Insert(session);
                if (response.InError)
                {
                    response.Merge(insertSessionResponse);
                    return response;
                }

                response.ResponseData = session.SessionKey;
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }

        public Response<bool> Logout()
        {
            Response<bool> response = new();

            try
            {
                string session = _httpAccessor.HttpContext.Request.Headers["Session"];
                if (string.IsNullOrWhiteSpace(session))
                {
                    response.AddValidationMessage("000","Usuário não está logado.");
                    return response;
                }

                response = _sessionRepository.DeleteByKey(session);
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }

        public Response<bool> ValidateSession(string session)
        {
            Response<bool> response = new();

            try
            {
                if(string.IsNullOrWhiteSpace(session))
                {
                    response.AddValidationMessage("001", "Faça login para continuar.");
                    return response;
                }

                Response<Session> sessionResponse = _sessionRepository.FindByKey(session);
                if (sessionResponse.InError)
                {
                    response.Merge(sessionResponse);
                    return response;
                }

                Session currentSession = sessionResponse.ResponseData;
                if(currentSession == null)
                {
                    response.AddValidationMessage("001", "Sessão inválida, faça login para continuar.");
                    return response;
                }

                if(currentSession.LastUse.AddHours(1) < DateTime.UtcNow)
                {
                    _sessionRepository.DeleteByKey(session);
                    response.AddValidationMessage("001","Sessão inválida, faça login para continuar.");
                    return response;
                }

                currentSession.LastUse = DateTime.UtcNow;
                response = _sessionRepository.Update(currentSession);

                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }

        public Response<RegisterUser> FindUserBySession()
        {
            Response<RegisterUser> response = new();

            string session = _httpAccessor.HttpContext.Request.Headers["Session"];
            try
            {
                if (string.IsNullOrWhiteSpace(session))
                {
                    response.AddWarningMessage("000","Session isn't supposed to be empty");
                    return response;
                }

                Response<Session> userSessionResponse = _sessionRepository.FindByKey(session);

                if (userSessionResponse.HasResponseData)
                {
                    response = _userRepository.FindById((int)userSessionResponse.ResponseData.Id_User);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
    }
}
