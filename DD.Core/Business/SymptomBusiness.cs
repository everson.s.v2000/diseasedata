﻿using System;
using DD.Core.Models;
using DD.Core.Repository;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;

namespace DD.Core.Business
{
    public class SymptomBusiness
    {
        private ILogger _logger;
        private IRepository _repository;
        private SymptomRepository _symptomRepository;

        public SymptomBusiness(IRepository repository, 
                               ILogger<SymptomBusiness> logger,
                               ILoggerFactory loggerFactory)
        {
            this._repository = repository;
            this._logger = logger;
            this._symptomRepository = new(repository, new Logger<SymptomRepository>(loggerFactory));
        }

        public Response<int> Insert(Symptom symptom)
        {
            Response<int> response = new();

            try
            {
                _repository.BeginTransaction();


                response = _symptomRepository.Insert(symptom);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<bool> Update(Symptom symptom)
        {
            Response<bool> response = new();

            try
            {
                _repository.BeginTransaction();

                response = _symptomRepository.Update(symptom);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<Symptom> FindById(int id)
        {
            Response<Symptom> response = new();
            try
            {
                response = _symptomRepository.FindById(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("801", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
    }
}
