﻿using DD.Core.Models;
using DD.Core.Repository;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Business
{
    public class PersonBusiness
    {
        private ILogger _logger;
        private IRepository _repository;
        private PersonRepository _personRepository;
        private AddressRepository _addressRepository;
        public PersonBusiness(IRepository repository,
                              ILogger<PersonBusiness> logger,
                              ILoggerFactory loggerFactory)
        {
            this._repository = repository;
            this._logger = logger;
            this._personRepository = new PersonRepository(repository, new Logger<PersonRepository>(loggerFactory));
            this._addressRepository = new AddressRepository(repository, new Logger<AddressRepository>(loggerFactory));
        }

        public Response<int> Insert(Person person)
        {
            Response<int> response = new();

            try
            {
                _repository.BeginTransaction();

                Response<int> addressResponse = _addressRepository.Insert(person.Address);
                if (addressResponse.InError)
                {
                    response.Merge(addressResponse);
                    return response;
                }
                person.Address.Id = addressResponse.ResponseData;


                response = _personRepository.Insert(person);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<bool> Update(Person person)
        {
            Response<bool> response = new();

            try
            {
                _repository.BeginTransaction();

                Response<bool> addressResponse = _addressRepository.Update(person.Address);
                if (addressResponse.InError)
                {
                    response.Merge(addressResponse);
                    return response;
                }

                response = _personRepository.Update(person);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<Person> FindById(int id)
        {
            Response<Person> response = new();
            try
            {
                response = _personRepository.FindById(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("801", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
    }
}
