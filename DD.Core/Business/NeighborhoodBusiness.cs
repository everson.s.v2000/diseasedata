﻿using DD.Core.Models;
using DD.Core.Repository;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Business
{
    public class NeighborhoodBusiness
    {
        private ILogger _logger;
        private IRepository _repository;
        private NeighborhoodRepository _NeighborhoodRepository;
        public NeighborhoodBusiness(IRepository repository, ILogger<NeighborhoodBusiness> logger, ILoggerFactory loggerFactory)
        {
            this._repository = repository;
            this._logger = logger;
            this._NeighborhoodRepository = new NeighborhoodRepository(_repository, new Logger<NeighborhoodRepository>(loggerFactory));
        }

        public InquiryResponse<Neighborhood> FindAll()
        {
            InquiryResponse<Neighborhood> response = new();
            try
            {
                response = this._NeighborhoodRepository.FindAll();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }
            return response;
        }

    }
}
