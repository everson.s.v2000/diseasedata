﻿using DD.Core.Integrity;
using DD.Core.Models;
using DD.Core.Models.DTO;
using DD.Core.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using Nedesk.Security;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Business
{
    public class UserBusiness
    {
        private ILogger _logger;
        private IRepository _repository;
        private UserRepository _userRepository;
        private AddressRepository _addressRepository;
        private Encrypter _encrypter;
        private UserIntegrity _userIntegrity;
        private readonly string privateKey;
        private readonly string publicKey;
        public UserBusiness(IRepository repository, 
                            ILogger<UserBusiness> logger, 
                            ILoggerFactory loggerFactory,
                            IConfiguration configuration,
                            Encrypter encrypter,
                            UserIntegrity userIntegrity)
        {
            _logger = logger;
            _repository = repository;
            _userRepository = new UserRepository(repository, new Logger<UserRepository>(loggerFactory));
            _addressRepository = new AddressRepository(repository, new Logger<AddressRepository>(loggerFactory));
            _encrypter = encrypter;
            _userIntegrity = userIntegrity;

            this.publicKey = configuration.GetSection("Keys")["Public"];
            this.privateKey = configuration.GetSection("Keys")["Private"];
        }

        public Response<int> Insert(DTO_RegisterUser user)
        {
            Response<int> response = new();
            _userIntegrity.ValidateInsert(user, response);
            if (response.HasValidationMessages)
                return response;


            try
            {
                _repository.BeginTransaction();


                user.Password = this._encrypter.GetEncrpytString(user.Password, publicKey, privateKey);

                Response<int> addressResponse = _addressRepository.Insert(user.Address);
                if (addressResponse.InError)
                {
                    response.Merge(addressResponse);
                    return response;
                }

                user.Address.Id = addressResponse.ResponseData;

                response = _userRepository.Insert(user);
                response.Merge(addressResponse);

                if (!response.InError)
                {
                    response.AddInformationMessage("001", "Usuário inserido com sucesso!");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<bool> Update(DTO_UpdateUser user)
        {
            Response<bool> response = new();

            _userIntegrity.ValidateUpdate(user, response);
            if (response.HasValidationMessages)
                return response;

            try
            {
                _repository.BeginTransaction();

                Response<RegisterUser> fetchUserByOldPasswordResponse = this._userRepository.FindByLogin(user.Email, this._encrypter.GetEncrpytString(user.OldPassword, publicKey, privateKey));
                if (fetchUserByOldPasswordResponse.InError)
                {
                    response.Merge(fetchUserByOldPasswordResponse);
                    return response;
                }

                if (!fetchUserByOldPasswordResponse.HasResponseData)
                {
                    response.AddValidationMessage("121", "Senha incorreta, utilize a senha anterior para atualizar o usuário.");
                    return response;
                }

                user.Password = this._encrypter.GetEncrpytString(user.Password, publicKey, privateKey);

                Response<bool> addressResponse = _addressRepository.Update(user.Address);
                if (addressResponse.InError)
                {
                    response.Merge(addressResponse);
                    return response;
                }

                response = _userRepository.Update(user);

                if (!response.InError)
                {
                    response.AddInformationMessage("000","Usuário atualizado com sucesso!");
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<RegisterUser> FindById(int id)
        {
            Response<RegisterUser> response = new();
            try
            {
                response = _userRepository.FindById(id);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("801",ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }

        public Response<RegisterUser> FindByEmail(string email)
        {
            Response<RegisterUser> response = new();
            try
            {
                ListRequest request = new();
                request.Filters.Add(new Filter
                {
                    Target1 = "U.Email",
                    Value1 = email,
                    OperationType = FilterOperationType.Equals
                });

                response = _userRepository.FindByRequest(request);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("801", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
    }
}
