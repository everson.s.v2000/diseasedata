﻿using DD.Core.Models;
using DD.Core.Repository;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Business
{
    public class DiseaseReportSummaryBusiness
    {
        private ILogger _logger;
        private IRepository _repository;

        private DiseaseReportSummaryRepository _diseaseReportSummaryRepository;

        public DiseaseReportSummaryBusiness(IRepository repository,
                                     ILogger<DiseaseReportSummaryBusiness> logger,
                                     ILoggerFactory loggerFactory)
        {
            this._repository = repository;
            this._logger = logger;
            this._diseaseReportSummaryRepository = new(repository, new Logger<DiseaseReportSummaryRepository>(loggerFactory));
        }

        public InquiryResponse<DiseaseReportSummary> FetchSummary(ListRequest request)
        {
            return this._diseaseReportSummaryRepository.FetchSummary(request);
        }


    }
}
