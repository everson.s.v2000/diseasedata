﻿using DD.Core.Cache;
using DD.Core.Models;
using DD.Core.Repository;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Business
{
    public class PathogenBusiness
    {
        private ILogger _logger;
        private IRepository _repository;
        private RoleCache _roleCache;
        private PathogenRepository _pathogenRepository;

        public PathogenBusiness(IRepository repository, 
                                ILogger<PathogenBusiness> logger, 
                                ILoggerFactory loggerFactory,
                                RoleCache _roleCache)
        {
            this._repository = repository;
            this._logger = logger;
            this._roleCache = _roleCache;
            _pathogenRepository = new(repository, new Logger<PathogenRepository>(loggerFactory));
        }

        public Response<int> Insert(Pathogen pathogen)
        {
            Response<int> response = new();

            try
            {
                _repository.BeginTransaction();


                response = _pathogenRepository.Insert(pathogen);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<bool> Update(Pathogen pathogen)
        {
            Response<bool> response = new();

            try
            {
                _repository.BeginTransaction();


                response = _pathogenRepository.Update(pathogen);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<Pathogen> FindById(int id)
        {
            Response<Pathogen> response = new();
            try
            {
                response = _pathogenRepository.FindById(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("801", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
    }
}
