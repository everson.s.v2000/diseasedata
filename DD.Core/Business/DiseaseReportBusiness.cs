﻿using DD.Core.Integrity;
using DD.Core.Models;
using DD.Core.Repository;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nedesk.Core.Interfaces;

namespace DD.Core.Business
{
    public class DiseaseReportBusiness
    {
        private ILogger _logger;
        private IRepository _repository;

        private PersonRepository _personRepository;
        private DiseaseReportRepository _diseaseReportRepository;
        private DiseaseReportStatusRepository _diseaseReportStatusRepository;
        private AddressRepository _addressRepository;
        private DiseaseReportIntegrity _diseaseReportIntegrity;
        public DiseaseReportBusiness(IRepository repository, 
                                     ILogger<DiseaseReportBusiness> logger,
                                     DiseaseReportIntegrity diseaseReportIntegrity,
                                     ILoggerFactory loggerFactory)
        {
            this._repository = repository;
            this._logger = logger;
            this._diseaseReportIntegrity = diseaseReportIntegrity;
            this._diseaseReportRepository = new(repository, new Logger<DiseaseReportRepository>(loggerFactory));
            this._diseaseReportStatusRepository = new(repository, new Logger<DiseaseReportStatusRepository>(loggerFactory));
            this._personRepository = new(repository, new Logger<PersonRepository>(loggerFactory));
            this._addressRepository = new(repository, new Logger<AddressRepository>(loggerFactory));
        }

        public Response<int> Insert(DiseaseReport diseaseReport)
        {
            Response<int> response = new();
            _diseaseReportIntegrity.ValidateInsert(diseaseReport,response);
            if (response.HasValidationMessages)
                return response;


            try
            {
                _repository.BeginTransaction();

                Response<int> addressResponse = _addressRepository.Insert(diseaseReport.InfectedPerson.Address);
                if (addressResponse.InError)
                {
                    response.Merge(addressResponse);
                    return response;
                }
                diseaseReport.InfectedPerson.Address.Id = addressResponse.ResponseData;

                var personResponse = _personRepository.Insert(diseaseReport.InfectedPerson);
                response.Merge(personResponse);

                if (personResponse.InError)
                {
                    return response;
                }

                response = _diseaseReportRepository.Insert(diseaseReport);
                if (response.InError)
                {
                    return response;
                }

                response.Merge(_diseaseReportStatusRepository.Save(diseaseReport));

                response.AddInformationMessage("001", "Reporte salvo com sucesso!");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<bool> Update(DiseaseReport diseaseReport)
        {
            Response<bool> response = new();
            _diseaseReportIntegrity.ValidateUpdate(diseaseReport, response);
            if (response.HasValidationMessages)
                return response;

            try
            {
                _repository.BeginTransaction();

                Response<bool> addressResponse = _addressRepository.Update(diseaseReport.InfectedPerson.Address);
                if (addressResponse.InError)
                {
                    response.Merge(addressResponse);
                    return response;
                }

                var personResponse = _personRepository.Update(diseaseReport.InfectedPerson);
                if (personResponse.InError)
                {
                    response.Merge(personResponse);
                    return response;
                }

                response = _diseaseReportRepository.Update(diseaseReport);
                if (response.InError)
                {
                    return response;
                }

                response.Merge(_diseaseReportStatusRepository.Save(diseaseReport));

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("800", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public Response<DiseaseReport> FindById(int id)
        {
            Response<DiseaseReport> response = new();
            try
            {
                response = _diseaseReportRepository.FindById(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("801", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
        public InquiryResponse<DiseaseReport> FindByRequest(ListRequest request)
        {
            InquiryResponse<DiseaseReport> response = new();
            try
            {
                if(request == null)
                {
                    _logger.LogDebug("Request provided on FindByRequest method was null");
                    return response;
                }
                
                response = _diseaseReportRepository.FindByRequest(request);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("801", ex.Message);
            }
            finally
            {
                _repository.CloseConnection();
            }

            return response;
        }
    }
}
