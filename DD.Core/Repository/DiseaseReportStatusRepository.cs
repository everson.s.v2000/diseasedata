﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class DiseaseReportStatusRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        #region SQL
        private const string INSERT =
@"INSERT INTO DiseaseReportStatus(Id_DiseaseReport,Id_InfectionSeverityType) VALUES(@Id_DiseaseReport,@Id_InfectionSeverityType);";

        private const string DELETE =
@"DELETE FROM DiseaseReportStatus WHERE Id_DiseaseReport = @Id_DiseaseReport;";
        #endregion

        public DiseaseReportStatusRepository(IRepository repository, ILogger<DiseaseReportStatusRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

      

        public Response<bool> Save(DiseaseReport diseaseReport)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id_DiseaseReport", diseaseReport.Id);

                using (DbCommand command = _repository.CreateCommand(DELETE, parameters))
                {
                    response = new Response<bool>();
                    _repository.ExecuteNonQuery(command);
                    response.ResponseData = true;
                }

                parameters.Clear();
                foreach (DiseaseReportStatus item in diseaseReport.ListStatus)
                {
                    parameters.Add("@Id_DiseaseReport", diseaseReport.Id);
                    parameters.Add("@Id_InfectionSeverityType", item.Id);

                    using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                    {
                        response = new Response<bool>();
                        _repository.ExecuteScalar(command);
                        response.ResponseData = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

    }
}
