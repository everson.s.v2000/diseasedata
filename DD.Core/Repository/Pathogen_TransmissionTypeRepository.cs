﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class Pathogen_TransmissionTypeRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        #region SQL
        private const string INSERT =
@"INSERT INTO Pathogen_TransmissionType(Id_Pathogen,Id_TransmissionType) VALUES(@Id_Pathogen,@Id_TransmissionType);";

        private const string DELETE =
@"DELETE FROM Pathogen_TransmissionType WHERE Id_Pathogen = @Id_Pathogen;";
        #endregion
        public Pathogen_TransmissionTypeRepository(IRepository repository, ILogger logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public Response<int> Insert(Pathogen pathogen)
        {
            Response<int> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                foreach(LookupType item in pathogen.TransmissionTypes)
                {
                    parameters.Add("@Id_Pathogen", pathogen.Id);
                    parameters.Add("@Id_TransmissionType", pathogen.Id);

                    using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                    {
                        response = new Response<int>();
                        response.ResponseData = _repository.ExecuteScalar(command);
                    }
                }
                
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        public Response<bool> Update(Pathogen pathogen)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id_Pathogen", pathogen.Id);

                using (DbCommand command = _repository.CreateCommand(DELETE, parameters))
                {
                    response = new Response<bool>();
                    _repository.ExecuteNonQuery(command);
                    response.ResponseData = true;
                }

                parameters.Clear();
                foreach (LookupType item in pathogen.TransmissionTypes)
                {
                    parameters.Add("@Id_Pathogen", pathogen.Id);
                    parameters.Add("@Id_TransmissionType", pathogen.Id);

                    using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                    {
                        response = new Response<bool>();
                        _repository.ExecuteNonQuery(command);
                        response.ResponseData = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
    }
}
