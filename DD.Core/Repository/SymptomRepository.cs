﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class SymptomRepository
    {
        private IRepository _repository;
        private ILogger _logger;
        #region SQL

        private const string INSERT =
@"INSERT INTO Symptom(Value,Description) VALUES(@Value,@Description);  SELECT LAST_INSERT_ID();";

        private const string UPDATE =
@"UPDATE Symptom SET Value = @Value, Description = @Description WHERE Id = @Id;";
        private const string SELECT =
@"SELECT Value,Description FROM Symptom";
        private const string DELETE =
@"DELETE FROM Symptom WHERE Id = @Id;";

        #endregion
        public SymptomRepository(IRepository repository, ILogger<SymptomRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public Response<int> Insert(Symptom symptom)
        {
            Response<int> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Value", symptom.Value);
                parameters.Add("@Description", symptom.Description);

                using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                {
                    response = new Response<int>();
                    response.ResponseData = _repository.ExecuteScalar(command);
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<bool> Update(Symptom symptom)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", symptom.Id);
                parameters.Add("@Value", symptom.Value);
                parameters.Add("@Description", symptom.Description);

                using (DbCommand command = _repository.CreateCommand(UPDATE, parameters))
                {
                    response = new Response<bool>();
                    _repository.ExecuteScalar(command);
                    response.ResponseData = true;
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<Symptom> FindById(int id)
        {
            Response<Symptom> response = new();
            try
            {

                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", id);

                StringBuilder whereClause = new();
                whereClause.AppendLine(" WHERE ");
                whereClause.AppendLine("Id = @Id");

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any Symptom with this id: {id}");
                            return response;
                        }

                        response.ResponseData = new();

                        while (reader.Read())
                            response.ResponseData = FillSymptom(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<bool> DeleteById(int id)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", id);

                using (DbCommand command = _repository.CreateCommand(DELETE, parameters))
                {
                    response = new Response<bool>();
                    _repository.ExecuteScalar(command);
                    response.ResponseData = true;
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        private Symptom FillSymptom(DbDataReader reader)
        {
            Symptom symptom = new();
            symptom.Id = reader["Id"] != DBNull.Value ? Convert.ToInt32(reader["Id"]) : 0;
            symptom.Value = reader["Value"] != DBNull.Value ? reader["Value"].ToString() : string.Empty;
            symptom.Description = reader["Description"] != DBNull.Value ? reader["Description"].ToString() : string.Empty;
            return symptom;
        }

    }
}
