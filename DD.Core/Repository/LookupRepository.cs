﻿using DD.Core.Enums;
using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class LookupTypeRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        #region SQL

        private const string SELECT =
@"SELECT Id, Name, Description, AdditionalInfo FROM ";

        private const string INSERT =
@"INSERT INTO {tableName}(Name,Description,AdditionalInfo) VALUES(@Id,@Name,@Description,@AdditionalInfo);";

        private const string UPDATE =
@"UPDATE {tableName} SET Name = @Name, Description = @Description, AdditionalInfo = @AdditionalInfo WHERE Id = @Id;";

        #endregion
        public LookupTypeRepository(IRepository repository, ILogger<LookupTypeRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public InquiryResponse<LookupType> FindAll(LookupEnum target)
        {
            InquiryResponse<LookupType> response = new();
            try
            {
                string sql = $"{SELECT}{target}";
                using (DbCommand command = _repository.CreateCommand(sql))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {

                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any lookup with the target: {target}");
                            return response;
                        }

                        response.ResponseData = new();

                        while (reader.Read())
                            response.ResponseData.Add(FillLookupType(reader));
                    }

                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        public Response<int> Insert(LookupEnum target,LookupType item)
        {
            Response<int> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Name", item.Name);
                parameters.Add("@Description", item.Description);
                parameters.Add("@AdditionalInfo", item.AdditionalInfo);

                string sql = INSERT.Replace("{tableName}", target.ToString());
                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    response = new Response<int>();
                    response.ResponseData = _repository.ExecuteScalar(command);
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        public Response<bool> Update(LookupEnum target, LookupType item)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Name", item.Name);
                parameters.Add("@Description", item.Description);
                parameters.Add("@AdditionalInfo", item.AdditionalInfo);

                string sql = UPDATE.Replace("{tableName}", target.ToString());
                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    response = new Response<bool>();
                    _repository.ExecuteNonQuery(command);
                    response.ResponseData = true;
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        private LookupType FillLookupType(DbDataReader reader)
        {
            LookupType item = new();

            item.Id = reader["Id"] != DBNull.Value ? Convert.ToInt32(reader["Id"]) : 0;
            item.Name = reader["Name"] != DBNull.Value ? reader["Name"].ToString() : string.Empty;
            item.Description = reader["Description"] != DBNull.Value ? reader["Description"].ToString() : string.Empty;
            item.AdditionalInfo = reader["AdditionalInfo"] != DBNull.Value ? reader["AdditionalInfo"].ToString() : string.Empty;

            return item;
        }

    }
}
