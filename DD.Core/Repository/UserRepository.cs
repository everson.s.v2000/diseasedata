﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.DataBase.Factory;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class UserRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        #region SQL

        private const string INSERT =
@"INSERT INTO User(Name,Email,Password,Phone,CellPhone,CellPhone_2,Id_Address) VALUES(@Name,@Email,@Password,@Phone,@CellPhone,@CellPhone_2,@Id_Address); SELECT LAST_INSERT_ID();";

        

        private const string UPDATE =
@"UPDATE User SET Name = @Name, Email = @Email, Password = @Password, Phone = @Phone, CellPhone = @CellPhone, CellPhone_2 = @CellPhone_2 WHERE Id = @Id;";

       

        private const string SELECT =
@"SELECT U.Id as UId,U.Name as UName, U.Email as UEmail, U.Password as UPassword, U.Phone as UPhone, U.CellPhone as UCellPhone, U.CellPhone_2 as UCellPhone_2,
A.Id as AId, A.Id_Neighborhood as AId_Neighborhood, A.Number as ANumber, A.Street as AStreet, A.Complement as AComplement, A.Reference as AReference
FROM 
User U INNER JOIN Address A on A.Id = U.Id_Address";

        #endregion

        public UserRepository(IRepository repository, ILogger<UserRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public Response<int> Insert(RegisterUser user)
        {
            Response<int> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Name", user.Name);
                parameters.Add("@Password", user.Password);
                parameters.Add("@Email", user.Email);
                parameters.Add("@Phone", user.Phone);
                parameters.Add("@Cellphone", user.Cellphone);
                parameters.Add("@Cellphone_2", user.Cellphone_2);
                parameters.Add("@Id_Address", user.Address.Id);

                using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                {
                    response = new Response<int>(); 
                    response.ResponseData = _repository.ExecuteScalar(command);
                }
            }
            catch(Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        public Response<bool> Update(RegisterUser user)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", user.Id);
                parameters.Add("@Name", user.Name);
                parameters.Add("@Password", user.Password);
                parameters.Add("@Email", user.Email);
                parameters.Add("@Phone", user.Phone);
                parameters.Add("@Cellphone", user.Cellphone);
                parameters.Add("@Cellphone_2", user.Cellphone_2);
                parameters.Add("@Id_Address", user.Address.Id);

                using (DbCommand command = _repository.CreateCommand(UPDATE, parameters))
                {
                    response = new Response<bool>();
                    _repository.ExecuteScalar(command);
                    response.ResponseData = true;
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        public Response<RegisterUser> FindByRequest(ListRequest request)
        {
            Response<RegisterUser> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = request.FilterParameters;

                StringBuilder whereClause = new($" WHERE {request.FilterWhereClause}");

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any User matching the request");
                            return response;
                        }

                        response.ResponseData = new();

                        while (reader.Read())
                            response.ResponseData = FillUser(reader);
                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        public Response<RegisterUser> FindById(int id)
        {
            Response<RegisterUser> response = new();
            try
            {

                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", id);

                StringBuilder whereClause = new();
                whereClause.AppendLine(" WHERE ");
                whereClause.AppendLine("U.Id = @Id");

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any User with this id: {id}");
                            return response;
                        }

                        response.ResponseData = new();

                        while (reader.Read())
                            response.ResponseData = FillUser(reader);
                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<RegisterUser> FindByLogin(string email, string password)
        {
            Response<RegisterUser> response = new();
            try
            {

                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Email", email);
                parameters.Add("@Password", password);

                StringBuilder whereClause = new();
                whereClause.AppendLine(" WHERE ");
                whereClause.AppendLine("U.Email = @Email AND");
                whereClause.AppendLine("U.Password = @Password");

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any User with this email: {email} and this password: {password}");
                            return response;
                        }

                        response.ResponseData = new();

                        while (reader.Read())
                            response.ResponseData = FillUser(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        private RegisterUser FillUser(DbDataReader reader)
        {
            RegisterUser item = new();

            item.Id = reader["UId"] != DBNull.Value ? Convert.ToInt32(reader["UId"]) : 0;
            item.Name = reader["UName"] != DBNull.Value ? reader["UName"].ToString() : string.Empty;
            item.Email = reader["UEmail"] != DBNull.Value ? reader["UEmail"].ToString() : string.Empty;
            item.Phone = reader["UPhone"] != DBNull.Value ? reader["UPhone"].ToString() : string.Empty;
            item.Cellphone = reader["UCellphone"] != DBNull.Value ? reader["UCellphone"].ToString() : string.Empty;
            item.Cellphone_2 = reader["UEmail"] != DBNull.Value ? reader["UEmail"].ToString() : string.Empty;

            
            item.Address = new();
            item.Address.Id = reader["AId"] != DBNull.Value ? Convert.ToInt32(reader["AId"]) : 0;
            item.Address.Id_Neighborhood = reader["AId_Neighborhood"] != DBNull.Value ? Convert.ToInt32(reader["AId_Neighborhood"]) : 0;
            item.Address.Number = reader["ANumber"] != DBNull.Value ? Convert.ToInt32(reader["ANumber"]) : 0;
            item.Address.Street = reader["AStreet"] != DBNull.Value ? reader["AStreet"].ToString() : string.Empty;
            item.Address.Complement = reader["AComplement"] != DBNull.Value ? reader["AComplement"].ToString() : string.Empty;
            item.Address.Reference = reader["AReference"] != DBNull.Value ? reader["AReference"].ToString() : string.Empty;

            return item;
        }

    }
}
