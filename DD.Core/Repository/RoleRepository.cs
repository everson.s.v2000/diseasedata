﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    public class RoleRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        #region SQL

        private const string SELECT =
@"SELECT Id, Name FROM Role";

        #endregion


        public RoleRepository(IRepository repository, ILogger<RoleRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public InquiryResponse<Role> FindAll()
        {
            InquiryResponse<Role> response = new();
            try
            {
                using (DbCommand command = _repository.CreateCommand(SELECT))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {

                        if (!reader.HasRows)
                        {
                            _logger.LogInformation("The database doesn't have any Role");
                            return response;
                        }

                        response.ResponseData = new();

                        while (reader.Read())
                            response.ResponseData.Add(FillRole(reader));
                    }

                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        private Role FillRole(DbDataReader reader)
        {
            Role item = new();

            item.Id = reader["Id"] != DBNull.Value ? Convert.ToInt32(reader["Id"]) : 0;
            item.Name = reader["Name"] != DBNull.Value ? reader["Name"].ToString() : string.Empty;

            return item;
        }
    }
}
