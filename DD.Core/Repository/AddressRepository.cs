﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class AddressRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        public AddressRepository(IRepository repository, ILogger<AddressRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        #region SQL

        private const string INSERT=
@"INSERT INTO Address(Id_Neighborhood,Number,Street,Complement,Reference) VALUES(@Id_Neighborhood,@Number,@Street,@Complement,@Reference); SELECT LAST_INSERT_ID();";

        private const string UPDATE =
@"UPDATE ADDRESS SET Id_Neighborhood = @Id_Neighborhood, Number = @Number, Street = @Street, Complement = @Complement, Reference = @Reference WHERE Id = @Id;";

        #endregion

        public Response<int> Insert(Address address)
        {
            Response<int> response = new();

            try
            {
                Dictionary<string, dynamic> addressParameters = new Dictionary<string, dynamic>();
                addressParameters.Add("@Id_Neighborhood", address.Id_Neighborhood);
                addressParameters.Add("@Number", address.Number);
                addressParameters.Add("@Street", address.Street);
                addressParameters.Add("@Complement", address.Complement);
                addressParameters.Add("@Reference", address.Reference);

                using (DbCommand command = _repository.CreateCommand(INSERT, addressParameters))
                {
                    response = new Response<int>();
                    response.ResponseData = _repository.ExecuteScalar(command);
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        public Response<bool> Update(Address address)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> addressParameters = new Dictionary<string, dynamic>();
                addressParameters.Add("@Id", address.Id);
                addressParameters.Add("@Id_Neighborhood", address.Id_Neighborhood);
                addressParameters.Add("@Number", address.Number);
                addressParameters.Add("@Street", address.Street);
                addressParameters.Add("@Complement", address.Complement);
                addressParameters.Add("@Reference", address.Reference);

                using (DbCommand command = _repository.CreateCommand(UPDATE, addressParameters))
                {
                    response = new Response<bool>();
                    _repository.ExecuteNonQuery(command);
                    response.ResponseData = true;
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
    }
}
