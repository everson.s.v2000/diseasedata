﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class PathogenRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        #region SQL

        private const string INSERT =
@"INSERT INTO Pathogen(Name,Id_PathogenType) VALUES(@Name,@Id_PathogenType); SELECT LAST_INSERT_ID();";

        private const string UPDATE =
@"UPDATE Pathogen SET Name = @Name, Id_PathogenType = @Id_PathogenType WHERE Id = @Id;";

        private const string SELECT =
@"SELECT P.Id as PId, P.Name as PName, P.Id_PathogenType,
PT.Id as PTId, PT.Name as PTName, PT.Description as PTDescription, PT.AdditionalInfo as PTAdditionalInfo,
TT.Id as TTId, TT.Name as TTName, TT.Description as TTDescription, TT.AdditionalInfo as TTAdditionalInfo
FROM Pathogen P
INNER JOIN Pathogen_TransmissionType PTT ON PTT.Id_Pathogen = P.Id
INNER JOIN TransmissionType TT ON TT.Id = PTT.Id_TransmissionType
INNER JOIN PathogenType PT ON P.Id_PathogenType = PT.Id";



        #endregion
        public PathogenRepository(IRepository repository, ILogger<PathogenRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public Response<int> Insert(Pathogen pathogen)
        {
            Response<int> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Name", pathogen.Name);
                parameters.Add("@Id_PathogenType", pathogen.Type.Id);

                using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                {
                    response = new Response<int>();
                    response.ResponseData = _repository.ExecuteScalar(command);
                }

            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }
            finally
            {
                _repository.EndTransaction();
            }

            return response;
        }
        public Response<bool> Update(Pathogen pathogen)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", pathogen.Id);
                parameters.Add("@Name", pathogen.Name);
                parameters.Add("@Id_PathogenType", pathogen.Type.Id);

                using (DbCommand command = _repository.CreateCommand(UPDATE, parameters))
                {
                    response = new Response<bool>();
                    _repository.ExecuteNonQuery(command);
                    response.ResponseData = true;
                }

                parameters.Clear();

                _repository.BeginTransaction();
               
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<Pathogen> FindById(int id)
        {
            Response<Pathogen> response = new();
            try
            {

                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", id);

                StringBuilder whereClause = new();
                whereClause.AppendLine(" WHERE ");
                whereClause.AppendLine("P.Id = @Id");

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation("The database doesn't have any pathogen with this id: {id}");
                            return response;
                        }

                        response.ResponseData = new();

                        if (reader.Read())
                            response.ResponseData = FillPathogen(reader);

                        while (reader.Read())
                            response.ResponseData.TransmissionTypes.Add(new LookupType
                            {
                                Id = reader["PTTId"] != DBNull.Value ? Convert.ToInt32(reader["PTTId"]) : 0,
                                Name = reader["PTTName"] != DBNull.Value ? reader["PTTName"].ToString() : string.Empty,
                                Description = reader["PTTDescription"] != DBNull.Value ? reader["PTTDescription"].ToString() : string.Empty,
                                AdditionalInfo = reader["PTTAdditionalInfo"] != DBNull.Value ? reader["PTTAdditionalInfo"].ToString() : string.Empty,
                            });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        private Pathogen FillPathogen(DbDataReader reader)
        {
            Pathogen pathogen = new();
            pathogen.Id = reader["PId"] != DBNull.Value ? Convert.ToInt32(reader["PId"]) : 0;
            pathogen.Name = reader["PName"] != DBNull.Value ? reader["PName"].ToString() : string.Empty;

            pathogen.Type = new()
            {
                Id = reader["PTId"] != DBNull.Value ? Convert.ToInt32(reader["PTId"]) : 0,
                Name = reader["PTName"] != DBNull.Value ? reader["PTName"].ToString() : string.Empty,
                Description = reader["PTDescription"] != DBNull.Value ? reader["PTDescription"].ToString() : string.Empty,
                AdditionalInfo = reader["PTAdditionalInfo"] != DBNull.Value ? reader["PTAdditionalInfo"].ToString() : string.Empty,
            };


            pathogen.TransmissionTypes = new()
            {
                new LookupType
                {
                    Id = reader["PTTId"] != DBNull.Value ? Convert.ToInt32(reader["PTTId"]) : 0,
                    Name = reader["PTTName"] != DBNull.Value ? reader["PTTName"].ToString() : string.Empty,
                    Description = reader["PTTDescription"] != DBNull.Value ? reader["PTTDescription"].ToString() : string.Empty,
                    AdditionalInfo = reader["PTTAdditionalInfo"] != DBNull.Value ? reader["PTTAdditionalInfo"].ToString() : string.Empty,
                }
            };

            return pathogen;
        }

        private List<Pathogen> FillPathogenList(DbDataReader reader)
        {
            List<Pathogen> pathogens = new();
            Pathogen pathogen = new();
            while (reader.Read())
            {
                if (reader["PId"] != DBNull.Value && pathogen.Id != Convert.ToInt32(reader["PId"]))
                {
                    pathogen.Id = Convert.ToInt32(reader["PId"]);
                    pathogen.Name = reader["PName"] != DBNull.Value ? reader["PName"].ToString() : string.Empty;
                    pathogen.Type = new()
                    {
                        Id = reader["PTId"] != DBNull.Value ? Convert.ToInt32(reader["PTId"]) : 0,
                        Name = reader["PTName"] != DBNull.Value ? reader["PTName"].ToString() : string.Empty,
                        Description = reader["PTDescription"] != DBNull.Value ? reader["PTDescription"].ToString() : string.Empty,
                        AdditionalInfo = reader["PTAdditionalInfo"] != DBNull.Value ? reader["PTAdditionalInfo"].ToString() : string.Empty,
                    };

                    pathogens.Add(pathogen);
                }

                pathogen.TransmissionTypes ??= new();
                pathogen.TransmissionTypes.Add(new LookupType
                {
                    Id = reader["PTTId"] != DBNull.Value ? Convert.ToInt32(reader["PTTId"]) : 0,
                    Name = reader["PTTName"] != DBNull.Value ? reader["PTTName"].ToString() : string.Empty,
                    Description = reader["PTTDescription"] != DBNull.Value ? reader["PTTDescription"].ToString() : string.Empty,
                    AdditionalInfo = reader["PTTAdditionalInfo"] != DBNull.Value ? reader["PTTAdditionalInfo"].ToString() : string.Empty,

                });

            }
            return pathogens;
        }
    }
}