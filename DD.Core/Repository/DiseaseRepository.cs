﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class DiseaseRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        #region SQL
        private const string INSERT =
@"INSERT INTO Disease(Id_Pathogen,Name,CycleOfLife,Treatment,TransmissionRate,DeathAverage) 
VALUES(@Id_Pathogen,@Name,@CycleOfLife,@Treatment,@TransmissionRate,@DeathAverage);";

        private const string UPDATE =
@"UPDATE Disease SET Name = @Name, Id_Pathogen = @Id_Pathogen, CycleOfLife = @CycleOfLife, Treatment = @Treatment, TransmissionRate = @TransmissionRate, DeathAverage = @DeathAverage WHERE Id = @Id;";

        private const string SELECT =
@"SELECT D.Id as DId,D.Id_Pathogen as DId_Pathogen,D.Name as DName,D.CycleOfLife as DCycleOfLife,
D.Treatment as DTreatment,D.TransmissionRate as DTransmissionRate,D.DeathAverage as DDeathAverage,
S.Id as SId, S.Value as SValue, S.Description as SDescription,
P.Id as PId, P.Name as PName, P.Id_PathogenType,
PT.ID as PTId, PT.Name as PTName, PT.Description as PTDescription, PT.AdditionalInfo as PTAdditionalInfo,
TT.Id as TTId, TT.Name as TTName, TT.Description as TTDescription, TT.AdditionalInfo as TTAdditionalInfo
FROM Disease D
LEFT JOIN Disease_Symptoms DS ON DS.Id_Disease = D.Id
LEFT JOIN Symptom S on S.Id = DS.Id_Symptom
LEFT JOIN Pathogen P on P.Id = D.Id_Pathogen
LEFT JOIN Pathogen_TransmissionType PTT ON PTT.Id_Pathogen = P.Id
LEFT JOIN TransmissionType TT ON TT.Id = PTT.Id_TransmissionType
LEFT JOIN PathogenType PT ON P.Id_PathogenType = PT.Id";
        private const string SELECT_COUNT =
@"SELECT COUNT(D.Id) as TotalItems
FROM Disease D
LEFT JOIN Disease_Symptoms DS ON DS.Id_Disease = D.Id
LEFT JOIN Symptom S on S.Id = DS.Id_Symptom
LEFT JOIN Pathogen P on P.Id = D.Id_Pathogen
LEFT JOIN Pathogen_TransmissionType PTT ON PTT.Id_Pathogen = P.Id
LEFT JOIN TransmissionType TT ON TT.Id = PTT.Id_TransmissionType
LEFT JOIN PathogenType PT ON P.Id_PathogenType = PT.Id";

        #endregion
        public DiseaseRepository(IRepository repository, ILogger<DiseaseRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public Response<int> Insert(Disease disease)
        {
            Response<int> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id_Pathogen", disease.DiseasePathogen.Id);
                parameters.Add("@Name", disease.Name);
                parameters.Add("@CycleOfLife", disease.CycleOfLife);
                parameters.Add("@Treatment", disease.Treatment);
                parameters.Add("@TransmissionRate", disease.TransmissionRate);
                parameters.Add("@DeathAverage", disease.DeathAverage);

                using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                {
                    response = new Response<int>();
                    response.ResponseData = _repository.ExecuteScalar(command);
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        public Response<bool> Update(Disease disease)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", disease.Id);
                parameters.Add("@Id_Pathogen", disease.DiseasePathogen.Id);
                parameters.Add("@Name", disease.Name);
                parameters.Add("@CycleOfLife", disease.CycleOfLife);
                parameters.Add("@Treatment", disease.Treatment);
                parameters.Add("@TransmissionRate", disease.TransmissionRate);
                parameters.Add("@DeathAverage", disease.DeathAverage);

                using (DbCommand command = _repository.CreateCommand(UPDATE, parameters))
                {
                    response = new Response<bool>();
                    _repository.ExecuteNonQuery(command);
                    response.ResponseData = true;
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<Disease> FindById(int id)
        {
            Response<Disease> response = new();
            try
            {

                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", id);

                StringBuilder whereClause = new();
                whereClause.AppendLine(" WHERE ");
                whereClause.AppendLine("D.Id = @Id");

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any Disease with this id: {id}");
                            return response;
                        }

                        response.ResponseData = new();

                        if(reader.Read())
                            response.ResponseData = FillDisease(reader);

                        while (reader.Read())
                        {
                            response.ResponseData.Symptoms.Add(new Symptom
                            {
                                Id = reader["SId"] != DBNull.Value ? Convert.ToInt32(reader["SId"]) : 0,
                                Value = reader["SValue"] != DBNull.Value ? reader["SValue"].ToString() : string.Empty,
                                Description = reader["SDescription"] != DBNull.Value ? reader["SDescription"].ToString() : string.Empty,
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public InquiryResponse<Disease> FetchAll(ListRequest request)
        {
            InquiryResponse<Disease> response = new();
            try
            {
                Dictionary<string, dynamic> parameters = new();
                StringBuilder whereClause = new();
                StringBuilder paginationClause = new();
                ManageRequestParameters(request, whereClause, paginationClause, parameters);

                string sql = $"{SELECT} {whereClause} {paginationClause}";

                using (DbCommand command = _repository.CreateCommand(sql,parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any Disease");
                            return response;
                        }

                        response.ResponseData = new();

                        if (reader.HasRows)
                            response.ResponseData = FillDiseaseList(reader);
                        else
                            response.ResponseData = new();

                    }
                }

                sql = $"{SELECT_COUNT} {whereClause}";
                 using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any Disease");
                            return response;
                        }


                        if (reader.HasRows)
                            if (reader.Read())
                            {
                                response.TotalItems = Convert.ToInt32(reader["TotalItems"]);
                                response.MergeFromRequest(request);
                            }
                        else
                            response.ResponseData = new();

                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        private void ManageRequestParameters(ListRequest request, StringBuilder whereClause, StringBuilder paginationClause, Dictionary<string, dynamic> parameters)
        {

            if (request.Limit > 0 && request.PageIndex > 0)
            {
                paginationClause.Append(request.GetOffSetAndLimit());

                Dictionary<string, int> paginationParameters = request.GetPaginationParameters();

                foreach (KeyValuePair<string, int> item in paginationParameters)
                {
                    parameters.Add(item.Key, item.Value);
                }

            }

            if (request.Filters.Any())
            {
                foreach (KeyValuePair<string, dynamic> item in request.FilterParameters)
                {
                    parameters.Add(item.Key, item.Value);
                }
                whereClause.Append($" WHERE {request.FilterWhereClause}");
            }
        }
        private Disease FillDisease(DbDataReader reader)
        {
            Disease disease = new();
            disease.Id = reader["DId"] != DBNull.Value ? Convert.ToInt32(reader["DId"]) : 0;
            disease.Name = reader["DName"] != DBNull.Value ? reader["DName"].ToString() : string.Empty;
            disease.CycleOfLife = reader["DCycleOfLife"] != DBNull.Value ? reader["DCycleOfLife"].ToString() : string.Empty;
            disease.Treatment = reader["DTreatment"] != DBNull.Value ? reader["DTreatment"].ToString() : string.Empty;
            disease.TransmissionRate = reader["DTransmissionRate"] != DBNull.Value ? Convert.ToDouble(reader["DTransmissionRate"]) : 0;
            disease.DeathAverage = reader["DDeathAverage"] != DBNull.Value ? Convert.ToDouble(reader["DDeathAverage"]) : 0;

            disease.DiseasePathogen = new()
            {
                Type = new()
                {
                    Id = reader["PTId"] != DBNull.Value ? Convert.ToInt32(reader["PTId"]) : 0,
                    Name = reader["PTName"] != DBNull.Value ? reader["PTName"].ToString() : string.Empty,
                    Description = reader["PTDescription"] != DBNull.Value ? reader["PTDescription"].ToString() : string.Empty,
                    AdditionalInfo = reader["PTAdditionalInfo"] != DBNull.Value ? reader["PTAdditionalInfo"].ToString() : string.Empty,
                },


                TransmissionTypes = new()
                {
                    new LookupType
                    {
                        Id = reader["TTId"] != DBNull.Value ? Convert.ToInt32(reader["TTId"]) : 0,
                        Name = reader["TTName"] != DBNull.Value ? reader["TTName"].ToString() : string.Empty,
                        Description = reader["TTDescription"] != DBNull.Value ? reader["TTDescription"].ToString() : string.Empty,
                        AdditionalInfo = reader["TTAdditionalInfo"] != DBNull.Value ? reader["TTAdditionalInfo"].ToString() : string.Empty,
                    }
                }
            };

            disease.Symptoms = new List<Symptom>()
            {
                new Symptom
                {
                    Id = reader["SId"] != DBNull.Value ? Convert.ToInt32(reader["SId"]) : 0,
                    Value = reader["SValue"] != DBNull.Value ? reader["SValue"].ToString() : string.Empty,
                    Description = reader["SDescription"] != DBNull.Value ? reader["SDescription"].ToString() : string.Empty,
                }
            };
            return disease;
        }

        private List<Disease> FillDiseaseList(DbDataReader reader)
        {
            List<Disease> diseases = new();
            Disease disease= new();
            while (reader.Read())
            {
               
                if (reader["DId"] != DBNull.Value && disease.Id != Convert.ToInt32(reader["DId"]))
                {
                    disease = new();
                    diseases.Add(disease);

                    disease.Id = reader["DId"] != DBNull.Value ? Convert.ToInt32(reader["DId"]) : 0;
                    disease.Name = reader["DName"] != DBNull.Value ? reader["DName"].ToString() : string.Empty;
                    disease.CycleOfLife = reader["DCycleOfLife"] != DBNull.Value ? reader["DCycleOfLife"].ToString() : string.Empty;
                    disease.Treatment = reader["DTreatment"] != DBNull.Value ? reader["DTreatment"].ToString() : string.Empty;
                    disease.TransmissionRate = reader["DTransmissionRate"] != DBNull.Value ? Convert.ToDouble(reader["DTransmissionRate"]) : 0;
                    disease.DeathAverage = reader["DDeathAverage"] != DBNull.Value ? Convert.ToDouble(reader["DDeathAverage"]) : 0;

                    disease.DiseasePathogen = new()
                    {
                        Type = new()
                        {
                            Id = reader["PTId"] != DBNull.Value ? Convert.ToInt32(reader["PTId"]) : 0,
                            Name = reader["PTName"] != DBNull.Value ? reader["PTName"].ToString() : string.Empty,
                            Description = reader["PTDescription"] != DBNull.Value ? reader["PTDescription"].ToString() : string.Empty,
                            AdditionalInfo = reader["PTAdditionalInfo"] != DBNull.Value ? reader["PTAdditionalInfo"].ToString() : string.Empty,
                        },


                        TransmissionTypes = new()
                        {
                            new LookupType
                            {
                                Id = reader["TTId"] != DBNull.Value ? Convert.ToInt32(reader["TTId"]) : 0,
                                Name = reader["TTName"] != DBNull.Value ? reader["TTName"].ToString() : string.Empty,
                                Description = reader["TTDescription"] != DBNull.Value ? reader["TTDescription"].ToString() : string.Empty,
                                AdditionalInfo = reader["TTAdditionalInfo"] != DBNull.Value ? reader["TTAdditionalInfo"].ToString() : string.Empty,
                            }
                        }
                    };

                }

                disease.Symptoms ??= new();
                disease.Symptoms.Add(new Symptom
                {
                    Id = reader["SId"] != DBNull.Value ? Convert.ToInt32(reader["SId"]) : 0,
                    Value = reader["SValue"] != DBNull.Value ? reader["SValue"].ToString() : string.Empty,
                    Description = reader["SDescription"] != DBNull.Value ? reader["SDescription"].ToString() : string.Empty,
                });

            }
            return diseases;
        }
    }
}
