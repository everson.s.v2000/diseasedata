﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class Disease_SymptomsRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        #region SQL

        private const string INSERT =
@"INSERT INTO Disease_SymptomsRepository(Id_Disease,Id_Symptom) VALUES(@Id_Disease,@Id_Symptom);";

        private const string DELETE =
@"DELETE FROM Disease_SymptomsRepository WHERE Id_Disease = @Id_Disease;";

        #endregion

        public Disease_SymptomsRepository(IRepository repository, ILogger logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public Response<int> Insert(Disease disease)
        {
            Response<int> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                foreach (Symptom item in disease.Symptoms)
                {
                    parameters.Add("@Id_Disease", disease.Id);
                    parameters.Add("@Id_Symptom", item.Id);

                    using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                    {
                        response = new Response<int>();
                        response.ResponseData = _repository.ExecuteScalar(command);
                    }
                }

            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        public Response<bool> Update(Disease disease)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id_Disease", disease.Id);

                using (DbCommand command = _repository.CreateCommand(DELETE, parameters))
                {
                    response = new Response<bool>();
                    _repository.ExecuteNonQuery(command);
                    response.ResponseData = true;
                }

                parameters.Clear();
                foreach (Symptom item in disease.Symptoms)
                {
                    parameters.Add("@Id_Disease", disease.Id);
                    parameters.Add("@Id_Symptom", item.Id);

                    using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                    {
                        response = new Response<bool>();
                        _repository.ExecuteScalar(command);
                        response.ResponseData = true;
                    }
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
    }
}
