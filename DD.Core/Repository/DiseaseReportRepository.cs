﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class DiseaseReportRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        #region SQL

        private const string INSERT =
@"INSERT Into DiseaseReport(Id_Disease,Id_InfectedPerson) VALUES(@Id_Disease,@Id_InfectedPerson); SELECT LAST_INSERT_ID()";

        private const string UPDATE =
@"UPDATE DiseaseReport SET Id_Disease = @Id_Disease, Id_InfectedPerson = @Id_InfectedPerson WHERE Id = @Id;";

        private const string SELECT =
@"SELECT 
DR.Id as DRId,
D.Id as DId,D.Id_Pathogen as DId_Pathogen,D.Name as DName,D.CycleOfLife as DCycleOfLife,
D.Treatment as DTreatment ,D.TransmissionRate as DTransmissionRate,D.DeathAverage as DDeathAverage,
S.Id as SId,S.Value as SValue, S.Description as SDescription,
P.Id as PId, P.Name as PName, P.Id_PathogenType,
PT.Id as PTId, PT.Name as PTName, PT.Description as PTDescription, PT.AdditionalInfo as PTAdditionalInfo,
TT.Id as TTId, TT.Name as TTName, TT.Description as TTDescription, TT.AdditionalInfo as TTAdditionalInfo,
PE.Id as PEId, PE.Name as PEName, PE.Id_DocumentType as PEId_DocumentType, PE.UniqueDocument as PEUniqueDocument, PE.Id_Address as PEId_Address,
A.Id as AId, A.Id_Neighborhood as AId_Neighborhood, A.Number as ANumber, A.Street as AStreet, A.Complement as AComplement, A.Reference as AReference,
DT.Id as DTId, DT.Name as DTName, DT.Description as DTDescription, DT.AdditionalInfo as DTAdditionalInfo,

DRS.Id_InfectionSeverityType as DRSId_InfectionSeverityType,
IST.Id as ISTId, IST.Name as ISTName, IST.Description as ISTDescription, IST.AdditionalInfo as ISTAdditionalInfo

FROM DiseaseReport DR
INNER JOIN Disease D ON D.Id = DR.Id_Disease
INNER JOIN DiseaseReportStatus DRS on DRS.Id_DiseaseReport = DR.Id
INNER JOIN Disease_Symptoms DS ON DS.Id_Disease = D.Id
INNER JOIN Symptom S on S.Id = DS.Id_Symptom
INNER JOIN Pathogen P on P.Id = D.Id_Pathogen
INNER JOIN Pathogen_TransmissionType PTT ON PTT.Id_Pathogen = P.Id
INNER JOIN TransmissionType TT ON TT.Id = PTT.Id_TransmissionType
INNER JOIN PathogenType PT ON P.Id_PathogenType = PT.Id
INNER JOIN Person PE ON DR.Id_InfectedPerson = PE.Id
INNER JOIN Address A on A.Id = PE.Id_Address
INNER JOIN DocumentType DT on DT.Id = PE.Id_DocumentType
INNER JOIN InfectionSeverityType IST on IST.Id = DRS.Id_InfectionSeverityType";

        #endregion

        public DiseaseReportRepository(IRepository repository, ILogger<DiseaseReportRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public Response<int> Insert(DiseaseReport diseaseReport)
        {
            Response<int> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id_Disease", diseaseReport.Disease.Id);
                parameters.Add("@Id_InfectedPerson", diseaseReport.InfectedPerson.Id);

                using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                {
                    response = new Response<int>();
                    response.ResponseData = _repository.ExecuteScalar(command);
                    diseaseReport.Id = response.ResponseData;
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<bool> Update(DiseaseReport diseaseReport)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", diseaseReport.Id);
                parameters.Add("@Id_Disease", diseaseReport.Disease.Id);
                parameters.Add("@Id_InfectedPerson", diseaseReport.InfectedPerson.Id);

                using (DbCommand command = _repository.CreateCommand(UPDATE, parameters))
                {
                    response = new Response<bool>();
                    _repository.ExecuteNonQuery(command);
                    response.ResponseData = true;
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<DiseaseReport> FindById(int id)
        {
            Response<DiseaseReport> response = new();
            try
            {

                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", id);

                StringBuilder whereClause = new();
                whereClause.AppendLine(" WHERE ");
                whereClause.AppendLine("DR.Id = @Id");

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any DiseaseReport with this id: {id}");
                            return response;
                        }

                        response.ResponseData = new();

                        if (reader.Read())
                            response.ResponseData = FillDiseaseReport(reader);

                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public InquiryResponse<DiseaseReport> FindByRequest(ListRequest request)
        {
            InquiryResponse<DiseaseReport> response = new();
            try
            {

                Dictionary<string, dynamic> parameters = request.FilterParameters;
                StringBuilder whereClause = new();
                if (request.Filters != null && request.Filters.Any())
                {
                    whereClause.AppendLine(" WHERE ");
                    whereClause.AppendLine(request.FilterWhereClause);
                }

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation("The database doesn't have any DiseaseReport with the request.");
                            return response;
                        }

                        response.ResponseData = new();

                        if (reader.Read())
                            response.ResponseData = FillDiseaseReportList(reader);

                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        private DiseaseReport FillDiseaseReport(DbDataReader reader)
        {
            DiseaseReport report = new();
            report.Id = reader["DRId"] != DBNull.Value ? Convert.ToInt32(reader["DRId"]) : 0;

            report.Disease = new();
            Disease disease = report.Disease;

            disease.Id = reader["DId"] != DBNull.Value ? Convert.ToInt32(reader["DId"]) : 0;
            disease.Name = reader["DName"] != DBNull.Value ? reader["DName"].ToString() : string.Empty;
            disease.CycleOfLife = reader["DCycleOfLife"] != DBNull.Value ? reader["DCycleOfLife"].ToString() : string.Empty;
            disease.Treatment = reader["DTreatment"] != DBNull.Value ? reader["DTreatment"].ToString() : string.Empty;
            disease.TransmissionRate = reader["DTransmissionRate"] != DBNull.Value ? Convert.ToDouble(reader["DTransmissionRate"]) : 0;
            disease.DeathAverage = reader["DDeathAverage"] != DBNull.Value ? Convert.ToDouble(reader["DDeathAverage"]) : 0;

            disease.DiseasePathogen = new()
            {
                Type = new()
                {
                    Id = reader["PTId"] != DBNull.Value ? Convert.ToInt32(reader["PTId"]) : 0,
                    Name = reader["PTName"] != DBNull.Value ? reader["PTName"].ToString() : string.Empty,
                    Description = reader["PTDescription"] != DBNull.Value ? reader["PTDescription"].ToString() : string.Empty,
                    AdditionalInfo = reader["PTAdditionalInfo"] != DBNull.Value ? reader["PTAdditionalInfo"].ToString() : string.Empty,
                },


                TransmissionTypes = new()
                {
                    new LookupType
                    {
                        Id = reader["TTId"] != DBNull.Value ? Convert.ToInt32(reader["TTId"]) : 0,
                        Name = reader["TTName"] != DBNull.Value ? reader["TTName"].ToString() : string.Empty,
                        Description = reader["TTDescription"] != DBNull.Value ? reader["TTDescription"].ToString() : string.Empty,
                        AdditionalInfo = reader["TTAdditionalInfo"] != DBNull.Value ? reader["TTAdditionalInfo"].ToString() : string.Empty,
                    }
                },

                Id = reader["PId"] != DBNull.Value ? Convert.ToInt32(reader["PId"]) : 0,
                Name = reader["PName"] != DBNull.Value ? reader["PName"].ToString() : string.Empty
            };

            disease.Symptoms ??= new List<Symptom>();
            disease.Symptoms.Add(new Symptom
            {
                Id = reader["SId"] != DBNull.Value ? Convert.ToInt32(reader["SId"]) : 0,
                Value = reader["SValue"] != DBNull.Value ? reader["SValue"].ToString() : string.Empty,
                Description = reader["SDescription"] != DBNull.Value ? reader["SDescription"].ToString() : string.Empty,
            });

            report.InfectedPerson = new();
            Person person = report.InfectedPerson;
            person.Id = reader["PEId"] != DBNull.Value ? Convert.ToInt32(reader["PEId"]) : 0;
            person.Name = reader["PEName"] != DBNull.Value ? reader["PEName"].ToString() : string.Empty;
            person.UniqueDocument = reader["PEUniqueDocument"] != DBNull.Value ? reader["PEUniqueDocument"].ToString() : string.Empty;

            person.Address = new();
            person.Address.Id = reader["AId"] != DBNull.Value ? Convert.ToInt32(reader["AId"]) : 0;
            person.Address.Id_Neighborhood = reader["AId_Neighborhood"] != DBNull.Value ? Convert.ToInt32(reader["AId_Neighborhood"]) : 0;
            person.Address.Number = reader["ANumber"] != DBNull.Value ? Convert.ToInt32(reader["ANumber"]) : 0;
            person.Address.Street = reader["AStreet"] != DBNull.Value ? reader["AStreet"].ToString() : string.Empty;
            person.Address.Complement = reader["AComplement"] != DBNull.Value ? reader["AComplement"].ToString() : string.Empty;
            person.Address.Reference = reader["AReference"] != DBNull.Value ? reader["AReference"].ToString() : string.Empty;

            person.DocumentType = new();
            person.DocumentType.Id = reader["DTId"] != DBNull.Value ? Convert.ToInt32(reader["DTId"]) : 0;
            person.DocumentType.Name = reader["DTName"] != DBNull.Value ? reader["DTName"].ToString() : string.Empty;
            person.DocumentType.Description = reader["DTDescription"] != DBNull.Value ? reader["DTDescription"].ToString() : string.Empty;
            person.DocumentType.AdditionalInfo = reader["DTAdditionalInfo"] != DBNull.Value ? reader["DTAdditionalInfo"].ToString() : string.Empty;

            report.ListStatus ??= new();
            report.ListStatus.Add(new DiseaseReportStatus
            {
                Id_DiseaseReport = reader["DRId"] != DBNull.Value ? Convert.ToInt32(reader["DRId"]) : 0,
                InfectionSeverityType = new()
                {
                    Id = reader["ISTId"] != DBNull.Value ? Convert.ToInt32(reader["ISTId"]) : 0,
                    Name = reader["ISTName"] != DBNull.Value ? reader["ISTName"].ToString() : string.Empty,
                    Description = reader["ISTDescription"] != DBNull.Value ? reader["ISTDescription"].ToString() : string.Empty,
                    AdditionalInfo = reader["ISTAdditionalInfo"] != DBNull.Value ? reader["ISTAdditionalInfo"].ToString() : string.Empty,
                },

            });
            return report;
        }

        private List<DiseaseReport> FillDiseaseReportList(DbDataReader reader)
        {
            List<DiseaseReport> reports = new();
            DiseaseReport report = new();

            while (reader.Read())
            {
                if (reader["DRId"] != DBNull.Value && report.Id != Convert.ToInt32(reader["DRId"]))
                {
                    report = new();
                    reports.Add(report);

                    report.Id = Convert.ToInt32(reader["DRId"]);

                    if (report.Disease == null)
                    {
                        report.Disease = new();
                        Disease disease = report.Disease;

                        disease.Id = reader["DId"] != DBNull.Value ? Convert.ToInt32(reader["DId"]) : 0;
                        disease.Name = reader["DName"] != DBNull.Value ? reader["DName"].ToString() : string.Empty;
                        disease.CycleOfLife = reader["DCycleOfLife"] != DBNull.Value ? reader["DCycleOfLife"].ToString() : string.Empty;
                        disease.Treatment = reader["DTreatment"] != DBNull.Value ? reader["DTreatment"].ToString() : string.Empty;
                        disease.TransmissionRate = reader["DTransmissionRate"] != DBNull.Value ? Convert.ToDouble(reader["DTransmissionRate"]) : 0;
                        disease.DeathAverage = reader["DDeathAverage"] != DBNull.Value ? Convert.ToDouble(reader["DDeathAverage"]) : 0;

                        disease.DiseasePathogen = new()
                        {
                            Type = new()
                            {
                                Id = reader["PTId"] != DBNull.Value ? Convert.ToInt32(reader["PTId"]) : 0,
                                Name = reader["PTName"] != DBNull.Value ? reader["PTName"].ToString() : string.Empty,
                                Description = reader["PTDescription"] != DBNull.Value ? reader["PTDescription"].ToString() : string.Empty,
                                AdditionalInfo = reader["PTAdditionalInfo"] != DBNull.Value ? reader["PTAdditionalInfo"].ToString() : string.Empty,
                            },


                            TransmissionTypes = new()
                            {
                                new LookupType
                                {
                                    Id = reader["TTId"] != DBNull.Value ? Convert.ToInt32(reader["TTId"]) : 0,
                                    Name = reader["TTName"] != DBNull.Value ? reader["TTName"].ToString() : string.Empty,
                                    Description = reader["TTDescription"] != DBNull.Value ? reader["TTDescription"].ToString() : string.Empty,
                                    AdditionalInfo = reader["TTAdditionalInfo"] != DBNull.Value ? reader["TTAdditionalInfo"].ToString() : string.Empty,
                                }
                            },

                            Id = reader["PId"] != DBNull.Value ? Convert.ToInt32(reader["PId"]) : 0,
                            Name = reader["PName"] != DBNull.Value ? reader["PName"].ToString() : string.Empty
                        };

                        disease.Symptoms ??= new List<Symptom>();
                        disease.Symptoms.Add(new Symptom
                        {
                            Id = reader["SId"] != DBNull.Value ? Convert.ToInt32(reader["SId"]) : 0,
                            Value = reader["SValue"] != DBNull.Value ? reader["SValue"].ToString() : string.Empty,
                            Description = reader["SDescription"] != DBNull.Value ? reader["SDescription"].ToString() : string.Empty,
                        });


                    }
                    if (report.InfectedPerson == null)
                    {
                        report.InfectedPerson = new();
                        Person person = report.InfectedPerson;

                        person.Id = reader["PEId"] != DBNull.Value ? Convert.ToInt32(reader["PEId"]) : 0;
                        person.Name = reader["PEName"] != DBNull.Value ? reader["PEName"].ToString() : string.Empty;
                        person.UniqueDocument = reader["PEUniqueDocument"] != DBNull.Value ? reader["PEUniqueDocument"].ToString() : string.Empty;

                        person.Address = new();
                        person.Address.Id = reader["AId"] != DBNull.Value ? Convert.ToInt32(reader["AId"]) : 0;
                        person.Address.Id_Neighborhood = reader["AId_Neighborhood"] != DBNull.Value ? Convert.ToInt32(reader["AId_Neighborhood"]) : 0;
                        person.Address.Number = reader["ANumber"] != DBNull.Value ? Convert.ToInt32(reader["ANumber"]) : 0;
                        person.Address.Street = reader["AStreet"] != DBNull.Value ? reader["AStreet"].ToString() : string.Empty;
                        person.Address.Complement = reader["AComplement"] != DBNull.Value ? reader["AComplement"].ToString() : string.Empty;
                        person.Address.Reference = reader["AReference"] != DBNull.Value ? reader["AReference"].ToString() : string.Empty;

                        person.DocumentType = new();
                        person.DocumentType.Id = reader["DTId"] != DBNull.Value ? Convert.ToInt32(reader["DTId"]) : 0;
                        person.DocumentType.Name = reader["DTName"] != DBNull.Value ? reader["DTName"].ToString() : string.Empty;
                        person.DocumentType.Description = reader["DTDescription"] != DBNull.Value ? reader["DTDescription"].ToString() : string.Empty;
                        person.DocumentType.AdditionalInfo = reader["DTAdditionalInfo"] != DBNull.Value ? reader["DTAdditionalInfo"].ToString() : string.Empty;


                    }

                }
                report.ListStatus ??= new();
                report.ListStatus.Add(new DiseaseReportStatus
                {
                    Id_DiseaseReport = reader["DRId"] != DBNull.Value ? Convert.ToInt32(reader["DRId"]) : 0,
                    InfectionSeverityType = new()
                    {
                        Id = reader["ISTId"] != DBNull.Value ? Convert.ToInt32(reader["ISTId"]) : 0,
                        Name = reader["ISTName"] != DBNull.Value ? reader["ISTName"].ToString() : string.Empty,
                        Description = reader["ISTDescription"] != DBNull.Value ? reader["ISTDescription"].ToString() : string.Empty,
                        AdditionalInfo = reader["ISTAdditionalInfo"] != DBNull.Value ? reader["ISTAdditionalInfo"].ToString() : string.Empty,
                    },

                });
            }

            return reports;

        }
    }
}
