﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class NeighborhoodRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        #region SQL
        private const string SELECT =
@"SELECT Id,Id_City,Name FROM Neighborhood";
        #endregion

        public NeighborhoodRepository(IRepository repository, ILogger<NeighborhoodRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public InquiryResponse<Neighborhood> FindAll()
        {
            InquiryResponse<Neighborhood> response = new();
            try
            {
                using (DbCommand command = _repository.CreateCommand(SELECT))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation("The database doesn't have any neighborhood");
                            return response;
                        }

                        response.ResponseData = new();

                        while (reader.Read())
                            response.ResponseData.Add(FillNeighborhood(reader));
                    }
                }
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }
            return response;
        }

        private Neighborhood FillNeighborhood(DbDataReader reader)
        {
            Neighborhood Neighborhood = new()
            {
                Id = reader["Id"] != DBNull.Value ? Convert.ToInt32(reader["Id"]) : 0,
                Id_City = reader["Id_City"] != DBNull.Value ? Convert.ToInt32(reader["Id_City"]) : 0,
                Name = reader["Name"] != DBNull.Value ? reader["Name"].ToString():string.Empty
            };
            return Neighborhood;
        }
    }
}
