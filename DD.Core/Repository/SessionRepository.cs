﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class SessionRepository
    {
        IRepository _repository;
        ILogger _logger;

        #region SQL

        public const string INSERT =
@"INSERT INTO Session(Id_User,SessionKey,LastUse) VALUES(@Id_User,@SessionKey,@LastUse); SELECT LAST_INSERT_ID();";

        public const string UPDATE =
@"UPDATE Session SET  SessionKey = @SessionKey, LastUse = @LastUse Where Id = @Id;";

        public const string SELECT =
@"SELECT Id, Id_User, SessionKey,LastUse FROM Session";

        public const string DELETE =
@"DELETE From Session";
        #endregion

        public SessionRepository(IRepository repository, ILogger<SessionRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public Response<int> Insert(Session session)
        {
            Response<int> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id_User", session.Id_User);
                parameters.Add("@SessionKey", session.SessionKey);
                parameters.Add("@LastUse", session.LastUse);

                using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                {
                    response.ResponseData = _repository.ExecuteScalar(command);
                }
            }
            catch(Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<bool> Update(Session session)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", session.Id);
                parameters.Add("@Id_User", session.Id_User);
                parameters.Add("@SessionKey", session.SessionKey);
                parameters.Add("@LastUse", session.LastUse);

                using (DbCommand command = _repository.CreateCommand(UPDATE, parameters))
                {
                    _repository.ExecuteScalar(command);
                    response.ResponseData = true; 
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<Session> FindById(long id)
        {
            Response<Session> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", id);

                StringBuilder whereClause = new();
                whereClause.AppendLine(" WHERE ");
                whereClause.AppendLine("Id = @Id");

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any Session with this id: {id}");
                            return response;
                        }

                        response.ResponseData = new();

                        while (reader.Read())
                            response.ResponseData = FillSession(reader);
                    }
                }
            }
            catch(Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<Session> FindByUserId(long id_user)
        {
            Response<Session> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id_User", id_user);

                StringBuilder whereClause = new();
                whereClause.AppendLine(" WHERE ");
                whereClause.AppendLine("Id_User = @Id_User");

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any Session with this id_user: {id_user}");
                            return response;
                        }

                        response.ResponseData = new();

                        while (reader.Read())
                            response.ResponseData = FillSession(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<Session> FindByKey(string SessionKey)
        {
            Response<Session> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@SessionKey", SessionKey);

                StringBuilder whereClause = new();
                whereClause.AppendLine(" WHERE ");
                whereClause.AppendLine("SessionKey = @SessionKey");

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any Session with this SessionKey: {SessionKey}");
                            return response;
                        }

                        response.ResponseData = new();

                        while (reader.Read())
                            response.ResponseData = FillSession(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<bool> DeleteByKey(string SessionKey)
        {
            Response<bool> response = new();
            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@SessionKey", SessionKey);

                StringBuilder whereClause = new();
                whereClause.AppendLine(" WHERE ");
                whereClause.AppendLine("SessionKey = @SessionKey");

                using (DbCommand command = _repository.CreateCommand(DELETE, parameters))
                {
                    _repository.ExecuteScalar(command);
                    response.ResponseData = true;
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }
        private Session FillSession(DbDataReader reader)
        {
            Session session = new();

            session.Id = reader["Id"] != DBNull.Value ? Convert.ToInt64(reader["Id"]) : 0;
            session.Id_User = reader["Id_User"] != DBNull.Value ? Convert.ToInt64(reader["Id_User"]) : 0;
            session.SessionKey = reader["SessionKey"] != DBNull.Value ? reader["SessionKey"].ToString() : string.Empty;
            session.LastUse = reader["LastUse"] != DBNull.Value ? Convert.ToDateTime(reader["LastUse"]) : DateTime.MinValue;

            return session;
        }
    }
}
