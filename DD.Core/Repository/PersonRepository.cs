﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    internal class PersonRepository
    {
        private IRepository _repository;
        private ILogger _logger;

        #region SQL
        private const string INSERT =
@"INSERT INTO Person(Name,Id_DocumentType,UniqueDocument,Id_Address) VALUES(@Name,@Id_DocumentType,@UniqueDocument,@Id_Address); SELECT LAST_INSERT_ID();";

        private const string UPDATE =
@"UPDATE Person SET Name = @Name, Id_DocumentType = @Id_DocumentType, UniqueDocument = @UniqueDocument, Id_Address = @Id_Address WHERE Id = @Id;";

        private const string SELECT =
@"SELECT P.Id as PId, P.Name as PName, P.Id_DocumentType as PId_DocumentType, P.UniqueDocument as PUniqueDocument, P.Id_Address as PId_Address,
A.Id as AId, A.Id_Neighborhood as AId_Neighborhood, A.Number as ANumber, A.Street as AStreet, A.Complement as AComplement, A.Reference as AReference,
D.Id as DId, D.Name as DName, D.Description as DDescription, D.AdditionalInfo as DAdditionalInfo
FROM 
Person P 
INNER JOIN Address A on A.Id = P.Id_Address
INNER JOIN DocumentType D on D.Id = P.Id_DocumentType";
        #endregion

        public PersonRepository(IRepository repository, ILogger<PersonRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }

        public Response<int> Insert(Person person)
        {
            Response<int> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Name", person.Name);
                parameters.Add("@Id_DocumentType", person.DocumentType.Id);
                parameters.Add("@UniqueDocument", person.UniqueDocument);
                parameters.Add("@Id_Address", person.Address.Id);

                using (DbCommand command = _repository.CreateCommand(INSERT, parameters))
                {
                    response.ResponseData = _repository.ExecuteScalar(command);
                    person.Id = response.ResponseData;
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<bool> Update(Person person)
        {
            Response<bool> response = new();

            try
            {
                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", person.Id);
                parameters.Add("@Name", person.Name);
                parameters.Add("@Id_DocumentType", person.DocumentType.Id);
                parameters.Add("@UniqueDocument", person.UniqueDocument);
                parameters.Add("@Id_Address", person.Address.Id);

                using (DbCommand command = _repository.CreateCommand(UPDATE, parameters))
                {
                    _repository.ExecuteScalar(command);
                    response.ResponseData = true;
                }
            }
            catch (Exception ex)
            {
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        public Response<Person> FindById(int id)
        {
            Response<Person> response = new();
            try
            {

                Dictionary<string, dynamic> parameters = new();
                parameters.Add("@Id", id);

                StringBuilder whereClause = new();
                whereClause.AppendLine(" WHERE ");
                whereClause.AppendLine("P.Id = @Id");

                string sql = $"{SELECT} {whereClause}";

                using (DbCommand command = _repository.CreateCommand(sql, parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation($"The database doesn't have any person with this id: {id}");
                            return response;
                        }

                        response.ResponseData = new();

                        while (reader.Read())
                            response.ResponseData = FillPerson(reader);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }

            return response;
        }

        private Person FillPerson(DbDataReader reader)
        {
            Person person = new();
            person.Id = reader["PId"] != DBNull.Value ? Convert.ToInt32(reader["PId"]) : 0;
            person.Name = reader["PName"] != DBNull.Value ? reader["PName"].ToString() : string.Empty;
            person.UniqueDocument = reader["PUniqueDocument"] != DBNull.Value ? reader["PUniqueDocument"].ToString() : string.Empty;

            
            person.Address = new();
            person.Address.Id = reader["AId"] != DBNull.Value ? Convert.ToInt32(reader["AId"]) : 0;
            person.Address.Id_Neighborhood = reader["AId_Neighborhood"] != DBNull.Value ? Convert.ToInt32(reader["AId_Neighborhood"]) : 0;
            person.Address.Number = reader["ANumber"] != DBNull.Value ? Convert.ToInt32(reader["ANumber"]) : 0;
            person.Address.Street = reader["AStreet"] != DBNull.Value ? reader["AStreet"].ToString() : string.Empty;
            person.Address.Complement = reader["AComplement"] != DBNull.Value ? reader["AComplement"].ToString() : string.Empty;
            person.Address.Reference = reader["AReference"] != DBNull.Value ? reader["AReference"].ToString() : string.Empty;

            person.DocumentType.Id = reader["DId"] != DBNull.Value ? Convert.ToInt32(reader["DId"]) : 0;
            person.DocumentType.Name = reader["DName"] != DBNull.Value ? reader["DName"].ToString() : string.Empty;
            person.DocumentType.Description = reader["DDescription"] != DBNull.Value ? reader["DDescription"].ToString() : string.Empty;
            person.DocumentType.AdditionalInfo = reader["DAdditionalInfo"] != DBNull.Value ? reader["DAdditionalInfo"].ToString() : string.Empty;
            return person;
        }
    }
}
