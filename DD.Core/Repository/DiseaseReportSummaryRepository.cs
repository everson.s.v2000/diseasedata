﻿using DD.Core.Models;
using Microsoft.Extensions.Logging;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DD.Core.Repository
{
    public class DiseaseReportSummaryRepository
    {

        private IRepository _repository;
        private ILogger _logger;

        #region SQL

        private const string SELECT =
@"SELECT D.Name DName, IST.Id ISTId, IST.Name ISTName, D.TransmissionRate, D.DeathAverage, N.Name Nname
FROM diseasedata.diseasereportstatus DRS
INNER JOIN DiseaseReport DR on DR.Id = DRS.Id_DiseaseReport
INNER JOIN Disease D on D.Id = DR.Id_Disease
INNER JOIN Person P on P.Id = DR.Id_InfectedPerson
INNER JOIN Address A on A.Id = P.Id_Address
INNER JOIN Neighborhood N on N.Id = A.Id_Neighborhood
INNER JOIN InfectionSeverityType IST on IST.Id = Id_InfectionSeverityType";

        private const string SELECT_COUNT =
@"SELECT COUNT(D.Name) TotalItems
FROM diseasedata.diseasereportstatus DRS
INNER JOIN DiseaseReport DR on DR.Id = DRS.Id_DiseaseReport
INNER JOIN Disease D on D.Id = DR.Id_Disease
INNER JOIN Person P on P.Id = DR.Id_InfectedPerson
INNER JOIN Address A on A.Id = P.Id_Address
INNER JOIN Neighborhood N on N.Id = A.Id_Neighborhood
INNER JOIN InfectionSeverityType IST on IST.Id = Id_InfectionSeverityType";

        #endregion
        public DiseaseReportSummaryRepository(IRepository repository, ILogger<DiseaseReportSummaryRepository> logger)
        {
            this._repository = repository;
            this._logger = logger;
        }


        public InquiryResponse<DiseaseReportSummary> FetchSummary(ListRequest request)
        {
            InquiryResponse<DiseaseReportSummary> response = new();
            try
            {
                Dictionary<string, dynamic> parameters = new();
                StringBuilder whereClause = new();
                StringBuilder paginationClause = new();

                ManageRequestParameters(request, whereClause, paginationClause, parameters);

                string sql = $"{SELECT} {whereClause} {paginationClause}";

                using (DbCommand command = _repository.CreateCommand(sql,parameters))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation("The database doesn't have any pathogen with this id: {id}");
                            return response;
                        }

                        response.ResponseData = fillSummary(reader);

                    }
                }

                sql = $"{SELECT_COUNT}";

                using (DbCommand command = _repository.CreateCommand(sql))
                {
                    BaseResponse<DbDataReader> readerResponse = _repository.ExecuteReader(command);
                    if (readerResponse.InError)
                    {
                        response.Merge(readerResponse);
                        return response;
                    }

                    using (DbDataReader reader = readerResponse.ResponseData)
                    {
                        if (!reader.HasRows)
                        {
                            _logger.LogInformation("The database doesn't have any pathogen with this id: {id}");
                            return response;
                        }

                        if (reader.Read())
                        {
                            response.TotalItems = Convert.ToInt32(reader["TotalItems"]);
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                response.AddExceptionMessage("999", ex.Message);
            }
            return response;
        }
        private void ManageRequestParameters(ListRequest request, StringBuilder whereClause, StringBuilder paginationClause, Dictionary<string, dynamic> parameters)
        {

            if (request.Limit > 0 && request.PageIndex > 0 && false)
            {
                paginationClause.Append(request.GetOffSetAndLimit());

                Dictionary<string, int> paginationParameters = request.GetPaginationParameters();

                foreach (KeyValuePair<string, int> item in paginationParameters)
                {
                    parameters.Add(item.Key, item.Value);
                }
            }

            if (request.Filters.Any())
            {
                foreach (KeyValuePair<string, dynamic> item in request.FilterParameters)
                {
                    parameters.Add(item.Key, item.Value);
                }
                whereClause.Append($" WHERE {request.FilterWhereClause}");
            }
        }
        private List<DiseaseReportSummary> fillSummary(DbDataReader reader)
        {
            List<DiseaseReportSummary> list = new();

            while (reader.Read())
            {
               
                DiseaseReportSummary item = list.Find(x => x.DiseaseName == reader["DName"].ToString());
                if (item == null)
                {
                    item = new();
                    list.Add(item);
                }
                item.NeighborhoodSummaries ??= new();

                string neighborhoodName = reader["NName"].ToString();
                NeighborhoodSummary nSummary = item.NeighborhoodSummaries.Find(x => x.NeighborhoodName == neighborhoodName);
                if (nSummary == null)
                {
                    nSummary = new();
                    nSummary.NeighborhoodName = neighborhoodName;
                    item.NeighborhoodSummaries.Add(nSummary);
                }
                nSummary.NumberOfCases++;
                IncreaseInfectionSeveirtyCount(nSummary, Convert.ToInt32(reader["ISTId"]));


                if (string.IsNullOrWhiteSpace(item.DiseaseName))
                    item.DiseaseName = reader["DName"].ToString();

                item.NumberOfCases += 1;

                if (item.TransmissionRate == 0)
                    item.TransmissionRate = Convert.ToDouble(reader["TransmissionRate"]);

                if (item.DeathAverage == 0)
                    item.DeathAverage = Convert.ToDouble(reader["DeathAverage"]);
                IncreaseInfectionSeveirtyCount(item, Convert.ToInt32(reader["ISTId"]));
            }

            return list;
        }

        private void IncreaseInfectionSeveirtyCount(DiseaseReportSummary item, int id)
        {
            _ = id switch
            {
                1 => item.VeryLowInfectionsSeverityCount++,
                2 => item.LowInfectionsSeverityCount++,
                3 => item.NormalInfectionsSeverityCount++,
                4 => item.HighInfectionsSeverityCount++,
                5 => item.VeryHighInfectionsSeverityCount++,
                _ => throw new NotImplementedException()
            };
        }

        private void IncreaseInfectionSeveirtyCount(NeighborhoodSummary item, int id)
        {
            _ = id switch
            {
                1 => item.VeryLowInfectionsSeverityCount++,
                2 => item.LowInfectionsSeverityCount++,
                3 => item.NormalInfectionsSeverityCount++,
                4 => item.HighInfectionsSeverityCount++,
                5 => item.VeryHighInfectionsSeverityCount++,
                _ => throw new NotImplementedException()
            };
        }
    }
}
