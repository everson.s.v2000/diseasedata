﻿namespace DD.Core.Enums
{
    public enum LookupEnum
    {
        DocumentType = 0,
        InfectionSeverityType = 1,
        PathogenType = 2,
        TransmissionType = 3
    }
}
