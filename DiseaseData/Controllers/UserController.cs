﻿using DD.Core.Business;
using DD.Core.Cache;
using DD.Core.ApiCore;
using DD.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using DD.Core.Models.DTO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DiseaseData.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : DDControllerBase
    {

        private readonly UserBusiness _userBusiness;
        private readonly RoleCache _roleCache;
        private readonly LookupTypeCache _lookupCache;

        public UserController(UserBusiness userBusiness,
                              DDAuth ddAuth,
                              RoleCache roleCache,
                              LookupTypeCache lookupTypeCache,
                              LoginBusiness loginBusiness,
                              IHttpContextAccessor httpContextAccessor) : base(loginBusiness,
                                                                                      ddAuth,
                                                                                      httpContextAccessor)
        {
            this._userBusiness = userBusiness;
            this._roleCache = roleCache;
            this._lookupCache = lookupTypeCache;
        }
        [HttpPost]
        public ActionResult<Response<int>> Insert([FromBody] DTO_RegisterUser user)
        {
            try
            {
                return base.GetResponse<int>(() => _userBusiness.Insert(user), requireAuthentication: false);
            }
            catch
            {
                return base.GetErrorResponse<int>();
            }
        }
        [HttpPut]
        public ActionResult<Response<bool>> Update([FromBody] DTO_UpdateUser user)
        {
            try
            {
                return base.GetResponse<bool>(() => _userBusiness.Update(user));
            }
            catch
            {
                return base.GetErrorResponse<bool>();
            }
        }
        [HttpGet]
        public ActionResult<Response<RegisterUser>> FetchById([FromQuery] int userId)
        {
            try
            {
                return base.GetResponse<RegisterUser>(() => _userBusiness.FindById(userId));
            }
            catch
            {
                return base.GetErrorResponse<RegisterUser>();
            }
        }

        [HttpGet("FindByEmail")]
        public ActionResult<Response<RegisterUser>> FindByEmail([FromQuery] string email)
        {
            try
            {
                return base.GetResponse<RegisterUser>(() => _userBusiness.FindByEmail(email), requireAuthentication:false) ;
            }
            catch
            {
                return base.GetErrorResponse<RegisterUser>();
            }
        }

       
    }
}
