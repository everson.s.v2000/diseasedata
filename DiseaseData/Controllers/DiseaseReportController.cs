﻿using DD.Core.Business;
using DD.Core.Cache;
using DD.Core.ApiCore;
using DD.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nedesk.Core.Models;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DiseaseData.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiseaseReportController : DDControllerBase
    {

        private readonly DiseaseReportBusiness _diseaseReportBusiness;
        private readonly DiseaseReportSummaryBusiness _diseaseReportSummaryBusiness;
        private readonly RoleCache _roleCache;
        private readonly LookupTypeCache _lookupCache;

        public DiseaseReportController(DiseaseReportBusiness diseaseReportBusiness,
                                       DiseaseReportSummaryBusiness diseaseReportSummaryBusiness,
                                       DDAuth ddAuth,
                                       RoleCache roleCache, 
                                       LookupTypeCache lookupTypeCache,
                                       LoginBusiness loginBusiness,
                                       IHttpContextAccessor httpContextAccessor):base(loginBusiness,
                                                                                      ddAuth,
                                                                                      httpContextAccessor)
        {
            this._diseaseReportBusiness = diseaseReportBusiness;
            this._diseaseReportSummaryBusiness = diseaseReportSummaryBusiness;
            this._roleCache = roleCache;
            this._lookupCache = lookupTypeCache;
        }
        [HttpPost]
        public ActionResult<Response<int>> Insert([FromBody] DiseaseReport diseaseReport)
        {
            try
            {

                return base.GetResponse<int>(() => _diseaseReportBusiness.Insert(diseaseReport));
            }
            catch
            {
                return base.GetErrorResponse<int>();
            }
        }
        [HttpPut]
        public ActionResult<Response<bool>> Update([FromBody] DiseaseReport diseaseReport)
        {
            try
            {
                return base.GetResponse<bool>(() => _diseaseReportBusiness.Update(diseaseReport));
            }
            catch
            {
                return base.GetErrorResponse<bool>();
            }
        }
        [HttpGet("FetchById")]
        public ActionResult<Response<DiseaseReport>> FetchById([FromQuery] int diseaseReportId)
        {
            try
            {
                return base.GetResponse<DiseaseReport>(() => _diseaseReportBusiness.FindById(diseaseReportId));
            }
            catch
            {
                return base.GetErrorResponse<DiseaseReport>();
            }
        }
        [HttpPost("FetchSummary")]
        public ActionResult<InquiryResponse<DiseaseReportSummary>> FetchSummary(ListRequest request)
        {
            try
            {
                return base.GetInquiryResponse<DiseaseReportSummary>(() => _diseaseReportSummaryBusiness.FetchSummary(request));
            }
            catch
            {
                return base.GetErrorInquiryResponse<DiseaseReportSummary>();
            }
        }
        //[HttpPost("FetchByRequest")]
        //public ActionResult<InquiryResponse<DiseaseReport>> FetchByRequest([FromBody] ListRequest request)
        //{
        //    try
        //    {
        //        return base.GetInquiryResponse<DiseaseReport>(() => _diseaseReportBusiness.FindByRequest(request));
        //    }
        //    catch
        //    {
        //        return base.GetErrorInquiryResponse<DiseaseReport>();
        //    }
        //}
    }
}
