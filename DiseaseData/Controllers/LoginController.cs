﻿using DD.Core.Business;
using DD.Core.Cache;
using DD.Core.ApiCore;
using DD.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DiseaseData.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : DDControllerBase
    {

        private readonly RoleCache _roleCache;
        private readonly LookupTypeCache _lookupCache;

        public LoginController(RoleCache roleCache, 
                               DDAuth ddAuth,
                               LookupTypeCache lookupTypeCache,
                               LoginBusiness loginBusiness,
                               IHttpContextAccessor httpContextAccessor) : base(loginBusiness,
                                                                                      ddAuth,
                                                                                      httpContextAccessor)
        {
            this._roleCache = roleCache;
            this._lookupCache = lookupTypeCache;
            
        }
        [HttpPost]
        public ActionResult<Response<string>> Login([FromBody] RegisterUser user)
        {
            try
            {
                return base.GetResponse<string>(() => _loginBusiness.Login(user), requireAuthentication: false);
            }
            catch
            {
                return base.GetErrorResponse<string>();
            }
        }
        [HttpGet("Logout")]
        public ActionResult<Response<bool>> Logout()
        {
            try
            {
                return base.GetResponse<bool>(() => _loginBusiness.Logout(), requireAuthentication: true);
            }
            catch
            {
                return base.GetErrorResponse<bool>();
            }
        }
        [HttpGet("FindUserBySession")]
        public ActionResult<Response<RegisterUser>> FindUserBySession()
        {
            try
            {
                return base.GetResponse<RegisterUser>(() => _loginBusiness.FindUserBySession(), requireAuthentication: true);
            }
            catch
            {
                return base.GetErrorResponse<RegisterUser>();
            }
        }
    }
}
