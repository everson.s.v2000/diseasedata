﻿using DD.Core.ApiCore;
using DD.Core.Business;
using DD.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiseaseData.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NeighborhoodController : DDControllerBase
    {
        private readonly NeighborhoodBusiness _neighborhoodBusiness;
        public NeighborhoodController(NeighborhoodBusiness neighborhodBusiness,
                                       DDAuth ddAuth,
                                       LoginBusiness loginBusiness,
                                       IHttpContextAccessor httpContextAccessor) : base(loginBusiness,
                                                                                      ddAuth,
                                                                                      httpContextAccessor)
        {
            this._neighborhoodBusiness = neighborhodBusiness;
        }

        [HttpGet]
        public ActionResult<InquiryResponse<Neighborhood>> FetchAll()
        {
            try
            {
                return GetInquiryResponse<Neighborhood>(() => _neighborhoodBusiness.FindAll(), requireAuthentication: false);
            }
            catch
            {
                return GetErrorInquiryResponse<Neighborhood>();
            }
        }
    }
}
