﻿using DD.Core.Business;
using DD.Core.Cache;
using DD.Core.ApiCore;
using DD.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nedesk.Core.Models;
using Microsoft.AspNetCore.Http;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DiseaseData.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiseaseController : DDControllerBase
    {

        private readonly DiseaseBusiness _diseaseBusiness;
        private readonly RoleCache _roleCache;
        private readonly LookupTypeCache _lookupCache;

        public DiseaseController(DiseaseBusiness diseaseReportBusiness,
                                       DDAuth ddAuth,
                                       RoleCache roleCache, 
                                       LookupTypeCache lookupTypeCache,
                                       LoginBusiness loginBusiness,
                                       IHttpContextAccessor httpContextAccessor):base(loginBusiness,
                                                                                      ddAuth,
                                                                                      httpContextAccessor)
        {
            this._diseaseBusiness = diseaseReportBusiness;
            this._roleCache = roleCache;
            this._lookupCache = lookupTypeCache;
        }
        [HttpPost]
        public ActionResult<Response<int>> Insert([FromBody] Disease disease)
        {
            try
            {

                return base.GetResponse<int>(() => _diseaseBusiness.Insert(disease));
            }
            catch
            {
                return base.GetErrorResponse<int>();
            }
        }
        [HttpPut]
        public ActionResult<Response<bool>> Update([FromBody] Disease disease)
        {
            try
            {
                return base.GetResponse<bool>(() => _diseaseBusiness.Update(disease));
            }
            catch
            {
                return base.GetErrorResponse<bool>();
            }
        }
        [HttpGet("fetchById")]
        public ActionResult<Response<Disease>> FetchById([FromQuery] int diseaseId)
        {
            try
            {
                return base.GetResponse<Disease>(() => _diseaseBusiness.FindById(diseaseId));
            }
            catch
            {
                return base.GetErrorResponse<Disease>();
            }
        }

        [HttpPost("FetchDiseases")]
        public ActionResult<InquiryResponse<Disease>> FetchAll(ListRequest request)
        {
            try
            {
                return base.GetInquiryResponse<Disease>(() => _diseaseBusiness.FetchAll(request));
            }
            catch
            {
                return base.GetErrorInquiryResponse<Disease>();
            }
        }
        //[HttpPost("FetchByRequest")]
        //public ActionResult<InquiryResponse<Disease>> FetchByRequest([FromBody] ListRequest request)
        //{
        //    try
        //    {
        //        return base.GetInquiryResponse<Disease>(() => _diseaseBusiness.FindByRequest(request));
        //    }
        //    catch
        //    {
        //        return base.GetErrorInquiryResponse<Disease>();
        //    }
        //}
    }
}
