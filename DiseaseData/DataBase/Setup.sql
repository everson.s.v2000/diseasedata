DROP SCHEMA IF EXISTS diseasedata;
CREATE SCHEMA `diseasedata` ;

USE diseasedata;

-- PRIMARY DATA

CREATE TABLE IF NOT EXISTS User(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Id_Address INT,
    Name VARCHAR(255),
    Email VARCHAR(255),
    Password VARCHAR(500),
    Phone VARCHAR(128),
    CellPhone VARCHAR(128),
    CellPhone_2 VARCHAR(128),
    SuperAdmin BIT DEFAULT 0,
    
	CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);

CREATE TABLE IF NOT EXISTS Session(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Id_User INT,
    SessionKey VARCHAR(500),
    LastUse DATETIME
);
CREATE TABLE IF NOT EXISTS Role(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS UserRole(
	Id_User INT,
    Id_Role INT
);

INSERT IGNORE INTO Role(Id,Name) 
VALUES
(1,'Default'),
(2,'Moderator'),
(3,'Admin');

-- LOCATION


CREATE TABLE IF NOT EXISTS Continent(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(255) UNIQUE,
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);
INSERT INTO Continent(name) VALUES('América do Sul');

CREATE TABLE IF NOT EXISTS Country(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Id_Continent INT,
    Name VARCHAR(255) UNIQUE,
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);
INSERT INTO Country(name, Id_Continent) VALUES('Brasil',1);

CREATE TABLE IF NOT EXISTS State(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Id_Country INT,
    Name VARCHAR(255) UNIQUE,
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);
INSERT INTO State(Name, Id_Country) VALUES('Pernambuco',1);

CREATE TABLE IF NOT EXISTS City(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Id_State INT,
    Name VARCHAR(255) UNIQUE,
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);
INSERT INTO City(Name, Id_State) VALUES('Recife',1);

CREATE TABLE IF NOT EXISTS Neighborhood(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Id_City INT,
    Name VARCHAR(255) UNIQUE,
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);
INSERT INTO Neighborhood(name,Id_city) VALUES('Barro',1);
INSERT INTO Neighborhood(name,Id_city) VALUES('Boa Vista',1);
INSERT INTO Neighborhood(name,Id_city) VALUES('Boa Viagem',1);
INSERT INTO Neighborhood(name,Id_city) VALUES('Cajueiro',1);
INSERT INTO Neighborhood(name,Id_city) VALUES('Casa Forte',1);
INSERT INTO Neighborhood(name,Id_city) VALUES('Espinheiro',1);
INSERT INTO Neighborhood(name,Id_city) VALUES('Graças',1);
INSERT INTO Neighborhood(name,Id_city) VALUES('Imbiribeira',1);
INSERT INTO Neighborhood(name,Id_city) VALUES('Poço da Panela',1);
INSERT INTO Neighborhood(name,Id_city) VALUES('Torre',1);

CREATE TABLE IF NOT EXISTS Address(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Id_Neighborhood INT,
    Number INT,
    Street VARCHAR(255),
    Complement VARCHAR(255),
    Reference VARCHAR(255),
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);


-- PATHOGEN

CREATE TABLE IF NOT EXISTS TransmissionType(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(255),
    Description VARCHAR(255),
    AdditionalInfo VARCHAR(255),
    
	CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);

INSERT INTO TransmissionType(Name,Description,AdditionalInfo) VALUES('Consume low quality Water','','');
INSERT INTO TransmissionType(Name,Description,AdditionalInfo) VALUES('General By Water','','');
INSERT INTO TransmissionType(Name,Description,AdditionalInfo) VALUES('Air','','');
INSERT INTO TransmissionType(Name,Description,AdditionalInfo) VALUES('Low quality air','','');
INSERT INTO TransmissionType(Name,Description,AdditionalInfo) VALUES('Wild Animals','','');

CREATE TABLE IF NOT EXISTS PathogenType(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(255),
    Description VARCHAR(255),
    AdditionalInfo VARCHAR(255),
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);

INSERT INTO PathogenType(Name,Description,AdditionalInfo) VALUES('Virus','','');
INSERT INTO PathogenType(Name,Description,AdditionalInfo) VALUES('bacterium','','');
INSERT INTO PathogenType(Name,Description,AdditionalInfo) VALUES('protozoan','','');
INSERT INTO PathogenType(Name,Description,AdditionalInfo) VALUES('Mind','','');

CREATE TABLE IF NOT EXISTS Pathogen(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Id_PathogenType INT,
    Name VARCHAR(255),
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);

CREATE TABLE IF NOT EXISTS Pathogen_TransmissionType(
	Id_Pathogen INT,
    Id_TransmissionType INT,
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);


-- DISEASE

CREATE TABLE IF NOT EXISTS Symptom(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Value VARCHAR(255),
    Description VARCHAR(255),
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);

CREATE TABLE IF NOT EXISTS Disease(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Id_Pathogen INT,
    Name VARCHAR(500),
    CycleOfLife VARCHAR(5000),
    Treatment VARCHAR(5000),
    TransmissionRate DECIMAL(13,2),
    DeathAverage DECIMAL(13,2),
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);
INSERT INTO Disease(Id_Pathogen,Name,CycleOfLife,Treatment,TransmissionRate,DeathAverage) VALUES(1,'Gripe','','',70,5);
INSERT INTO Disease(Id_Pathogen,Name,CycleOfLife,Treatment,TransmissionRate,DeathAverage) VALUES(1,'Malária','','',30,20);
INSERT INTO Disease(Id_Pathogen,Name,CycleOfLife,Treatment,TransmissionRate,DeathAverage) VALUES(1,'Covid-19','','',80,15);
INSERT INTO Disease(Id_Pathogen,Name,CycleOfLife,Treatment,TransmissionRate,DeathAverage) VALUES(1,'Dengue','','',32,20);

CREATE TABLE IF NOT EXISTS Disease_Symptoms(
	Id_Disease INT,
    Id_Symptom INT,
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);

-- Person


CREATE TABLE IF NOT EXISTS DocumentType(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(255),
    Description VARCHAR(255),
    AdditionalInfo VARCHAR(255),
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);

INSERT INTO DocumentType(Name,Description,AdditionalInfo) VALUES('CPF','','');
INSERT INTO DocumentType(Name,Description,AdditionalInfo) VALUES('RG','','');

CREATE TABLE IF NOT EXISTS Person(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(255),
    Id_DocumentType INT,
    UniqueDocument VARCHAR(255),
    Id_Address INT,
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);

-- DiseaseReport

CREATE TABLE IF NOT EXISTS InfectionSeverityType(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Name VARCHAR(255),
    Description VARCHAR(255),
    AdditionalInfo VARCHAR(255),
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);
INSERT INTO InfectionSeverityType(Name,Description,AdditionalInfo) VALUES('Very low','','');
INSERT INTO InfectionSeverityType(Name,Description,AdditionalInfo) VALUES('Low','','');
INSERT INTO InfectionSeverityType(Name,Description,AdditionalInfo) VALUES('Medium','','');
INSERT INTO InfectionSeverityType(Name,Description,AdditionalInfo) VALUES('High','','');
INSERT INTO InfectionSeverityType(Name,Description,AdditionalInfo) VALUES('Very high','','');

CREATE TABLE IF NOT EXISTS DiseaseReport(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Id_Disease INT,
    Id_InfectedPerson INT,
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME

);

CREATE TABLE IF NOT EXISTS DiseaseReportStatus(
	Id_DiseaseReport INT,
    Id_InfectionSeverityType INT,
    
    CreatedBy VARCHAR(100),
    CreatedOn DATETIME,
    ModifiedBy VARCHAR(100),
    ModifiedOn DATETIME
);

