using DD.Core.ApiCore;
using DD.Core.Business;
using DD.Core.Cache;
using DD.Core.Factory;
using DD.Core.Integrity;
using DD.Core.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Nedesk.Core.DataBase.Abstractions;
using Nedesk.Core.DataBase.Factory;
using Nedesk.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DiseaseData
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddLogging();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "DiseaseData", Version = "v1" });
            });
            services.AddCors(x => x.AddDefaultPolicy(s => s.SetIsOriginAllowedToAllowWildcardSubdomains().
                WithOrigins("http://localhost:4200/","http://52.179.121.49/", "http://munisaude.eastus.cloudapp.azure.com/", "http://munisaude.eastus.cloudapp.azure.com").AllowAnyMethod().AllowAnyHeader().DisallowCredentials().SetIsOriginAllowed((host) => true)));

            services.AddScoped<IDBConnectionFactory, MySqlConnectionFactory>(x => new MySqlConnectionFactory(Configuration.GetConnectionString("Default")));
            services.AddScoped<IRepository, Repository>();
            ConfigureInjections(services);
        }

        public void ConfigureInjections(IServiceCollection services)
        {
            // Core
            ConfigureCache(services);
            services.AddHttpContextAccessor();
            services.AddSingleton<Encrypter>();
            services.AddSingleton<SessionFactory>();
            services.AddScoped<DDAuth>();

            // business
            services.AddScoped<DiseaseBusiness>();
            services.AddScoped<DiseaseReportBusiness>();
            services.AddScoped<LoginBusiness>();
            services.AddScoped<LookupBusiness>();
            services.AddScoped<NeighborhoodBusiness>();
            services.AddScoped<PathogenBusiness>();
            services.AddScoped<PersonBusiness>();
            services.AddScoped<RoleBusiness>();
            services.AddScoped<SymptomBusiness>();
            services.AddScoped<UserBusiness>();
            services.AddScoped<DiseaseReportSummaryBusiness>();

            // Integrity
            services.AddSingleton<DiseaseIntegrity>();
            services.AddSingleton<DiseaseReportIntegrity>();
            services.AddSingleton<UserIntegrity>();

        }

        private void ConfigureCache(IServiceCollection services)
        {
            ServiceProvider serviceProvider = services.BuildServiceProvider();
            ILoggerFactory loggerFactory = serviceProvider.GetService<ILoggerFactory>();

            Repository repository = new Repository(new MySqlConnectionFactory(Configuration.GetConnectionString("Default")),new Logger<Repository>(loggerFactory));

            services.AddSingleton<RoleCache>(x => new RoleCache(new Logger<RoleCache>(loggerFactory),repository,loggerFactory));
            services.AddSingleton<LookupTypeCache>(x => new LookupTypeCache(new Logger<LookupTypeCache>(loggerFactory),repository,loggerFactory));

        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "DiseaseData v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
