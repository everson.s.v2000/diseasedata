﻿using Nedesk.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Nedesk.Core.Enums;
using System.Data.Common;

namespace Nedesk.Core.Models
{
    public class BaseResponse<T> : IBaseResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public T ResponseData { get; set; }
        public List<Message> Messages { get; set; } = new();
        public bool InError
        {
            get
            {
                return this.Messages.Exists(x => (int)x.MessageType >= 4);
            }
        }
        public bool HasAnyMessages
        {
            get
            {
                return Messages.Any();
            }
        }
        public bool HasCautionMessages
        {
            get
            {
                return Messages.FindAll(msg => msg.MessageType == Enums.MessageTypeEnum.Caution).Any();
            }
        }
        public bool HasValidationMessages
        {
            get
            {
                return Messages.FindAll(msg => msg.MessageType == Enums.MessageTypeEnum.Validation).Any();
            }
        }
        public bool HasWarningMessages
        {
            get
            {
                return Messages.FindAll(msg => msg.MessageType == Enums.MessageTypeEnum.Warning).Any();
            }
        }
        public bool HasErrorMessages
        {
            get
            {
                return Messages.FindAll(msg => msg.MessageType == Enums.MessageTypeEnum.Error).Any();
            }
        }
        public bool HasExceptionMessages
        {
            get
            {
                return Messages.FindAll(msg => msg.MessageType == Enums.MessageTypeEnum.Exception).Any();
            }
        }
        public bool HasFatalErrorMessages
        {
            get
            {
                return Messages.FindAll(msg => msg.MessageType == Enums.MessageTypeEnum.FatalError).Any();
            }
        }
        public bool HasResponseData
        {
            get
            {
                return ResponseData != null;
            }
        }

        public void AddCautionMessage(string code, string text)
        {
            this.Messages.Add(new Message
            {
                Code = code,
                MessageType = MessageTypeEnum.Caution,
                Text = text
            });
        }

        public void AddErrorMessage(string code, string text)
        {
            this.Messages.Add(new Message
            {
                Code = code,
                MessageType = MessageTypeEnum.Error,
                Text = text
            });
        }

        public void AddExceptionMessage(string code, string text)
        {
            this.Messages.Add(new Message
            {
                Code = code,
                MessageType = MessageTypeEnum.Exception,
                Text = text
            });
        }

        public void AddFatalErrorMessage(string code, string text)
        {
            this.Messages.Add(new Message
            {
                Code = code,
                MessageType = MessageTypeEnum.FatalError,
                Text = text
            });
        }

        public void AddInformationMessage(string code, string text)
        {
            this.Messages.Add(new Message
            {
                Code = code,
                MessageType = MessageTypeEnum.Information,
                Text = text
            });
        }

        public void AddValidationMessage(string code, string text)
        {
            this.Messages.Add(new Message
            {
                Code = code,
                MessageType = MessageTypeEnum.Validation,
                Text = text
            });
        }

        public void AddWarningMessage(string code, string text)
        {
            this.Messages.Add(new Message
            {
                Code = code,
                MessageType = MessageTypeEnum.Warning,
                Text = text
            });
        }

        public void Merge(IBaseResponse response)
        {
            this.Messages.AddRange(response.Messages);
        }

    }
}
