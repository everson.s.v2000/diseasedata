﻿using Nedesk.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Nedesk.Core.Models
{
    public class ListResponse<T> : BaseResponse<List<T>>
    {
        public int TotalItemsOnPage { get; set; }
        public int PageIndex { get; set; }
        public int TotalItems { get; set; }
    }
}
