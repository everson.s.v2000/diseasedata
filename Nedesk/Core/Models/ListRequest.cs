﻿using Nedesk.Core.Interfaces;
using Nedesk.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Nedesk.Core.Models
{
    public class ListRequest:BaseRequest
    {
        public int Limit { get; set; }
        public int PageIndex { get; set; }
        public List<Filter> Filters { get; set; } = new();

        private Dictionary<string, dynamic> _parameters;
        public Dictionary<string,dynamic> FilterParameters
        {
            get
            {
                _parameters ??= new();
                if (Filters == null) return _parameters;
                foreach(Filter filter in Filters)
                {
                    if (_parameters.ContainsKey($"@{filter.Target1}")) continue;

                    _parameters.Add($"@{filter.Target1.TargetAsParameter()}", filter.Value1);
                    if(filter.Value2 != null)
                        _parameters.Add($"@{filter.Target2.TargetAsParameter()}", filter.Value2);

                }
                return _parameters;
            }
        }
        public string FilterWhereClause
        {
            get
            {
                StringBuilder whereClause = new();
                if (Filters == null) return whereClause.ToString();

                List<Filter> sortedFilters = this.Filters.OrderBy(x => x.FilterGroup).ToList();
                bool EncapsulateByGroup = sortedFilters.Exists(f => f.FilterGroup > 0);
                int lastGroupId = 0;
                foreach (Filter filter in sortedFilters)
                {
                    if (EncapsulateByGroup)
                    {
                        if (filter.FilterGroup > lastGroupId)
                        {
                            lastGroupId = filter.FilterGroup;
                            whereClause.Append("(");
                        }
                        bool includeAggregateInsideGroup = sortedFilters.IndexOf(filter) < sortedFilters.FindLastIndex(f => f.FilterGroup == lastGroupId);
                        bool includeAggregateOutsideGroup = sortedFilters.IndexOf(filter) < sortedFilters.Count - 1;
                        whereClause.Append(GetWhereSnippet(filter, includeAggregateInsideGroup));

                        if (!includeAggregateInsideGroup)
                        {
                            whereClause.Append(")");
                            if (includeAggregateOutsideGroup)
                                whereClause.Append($" {filter.GetAggregateType()}");
                        }

                    }
                    else
                    {
                        whereClause.Append(GetWhereSnippet(filter, true));
                    }
                }

                return whereClause.ToString();
            }
        }

        public string GetWhereSnippet(Filter filter, bool includeAggregate)
        {
            string snippet = string.Empty;

            if (filter.Value2 != null)
            {
                if(filter.OperationType == FilterOperationType.Between)
                {
                    snippet = $"{filter.Target1} {filter.GetOperationType()} @{filter.Target1.TargetAsParameter()} AND @{filter.Target2.TargetAsParameter()}";
                    return snippet;
                }
            }
            
            snippet = $"{filter.Target1} {filter.GetOperationType()} @{filter.Target1.TargetAsParameter()}";
            return snippet;
        }

        public string GetOffSetAndLimit()
        {
            return $"LIMIT @Limit OFFSET @Offset";
        }

        public Dictionary<string,int> GetPaginationParameters()
        {
            Dictionary<string, int> parameters = new();
            parameters.Add("@Limit", this.Limit);
            parameters.Add("@Offset", (this.PageIndex - 1) * this.Limit);

            return parameters;
        }

    }
}
