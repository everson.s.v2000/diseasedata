﻿using Nedesk.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedesk.Core.Models
{
    public class Message
    {
        public string Code { get; set; }
        public MessageTypeEnum MessageType { get; set; }
        public string Text { get; set; }
    }
}
