﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Nedesk.Core.Models
{
    public class BaseModel
    {
        public int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public DateTime StartCreatedDate { get; set; }
        public DateTime EndCreatedDate { get; set; }

        public DateTime StartModifieddDate { get; set; }
        public DateTime EndModifiedDate { get; set; }


     
    }
}
