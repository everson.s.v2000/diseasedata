﻿using Nedesk.Core.Models;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace Nedesk.Core.DataBase.Abstractions
{
    public interface IRepository
    {
        BaseResponse<bool> OpenConnection();
        BaseResponse<bool> CloseConnection();
        BaseResponse<bool> BeginTransaction();
        BaseResponse<bool> EndTransaction();
        DbCommand CreateCommand(string sql, Dictionary<string, dynamic> parameters = null);
        void ExecuteNonQuery(DbCommand command);
        Task ExecuteNonQueryAsync(DbCommand command);
        BaseResponse<DbDataReader>ExecuteReader(DbCommand command);
        Task<DbDataReader> ExecuteReaderAsync(DbCommand command);
        int ExecuteScalar(DbCommand command);
        Task<int> ExecuteScalarAsync(DbCommand command);
        dynamic ExecuteScalarDynamic(DbCommand command);
        dynamic ExecuteScalarDynamicAsync(DbCommand command);
    }
}