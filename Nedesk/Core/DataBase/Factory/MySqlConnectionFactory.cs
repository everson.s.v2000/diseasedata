﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedesk.Core.DataBase.Factory
{
    public class MySqlConnectionFactory : IDBConnectionFactory
    {
        private MySqlConnection Connection;
        private string _connectionString;

        public MySqlConnectionFactory(string _connectionString)
        {
            this._connectionString = _connectionString;
        }

        public DbConnection GetConnection()
        {
            if (this.Connection != null && this.Connection.State is >= ConnectionState.Open and < ConnectionState.Broken) return this.Connection;

            this.Connection = new MySqlConnection(_connectionString);
            return this.Connection;
        }

    }
}
