﻿using System.Data.Common;

namespace Nedesk.Core.DataBase.Factory
{
    public interface IDBConnectionFactory
    {
        DbConnection GetConnection();
    }
}