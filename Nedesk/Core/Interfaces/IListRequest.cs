﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedesk.Core.Interfaces
{
    public interface IListRequest:IBaseRequest
    {
        public int Limit { get; set; }
        public int PageIndex { get; set; }
    }
}
