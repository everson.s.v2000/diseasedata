﻿using Nedesk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Nedesk.Core.Interfaces
{
    public interface IBaseResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public List<Message> Messages { get; set; }
        public bool InError { get;}
        public bool HasAnyMessages { get; }
        public bool HasCautionMessages { get; }
        public bool HasValidationMessages { get; }
        public bool HasWarningMessages { get; }
        public bool HasErrorMessages { get; }
        public bool HasExceptionMessages { get; }
        public bool HasFatalErrorMessages { get; }

        public void AddInformationMessage(string code, string text);
        public void AddCautionMessage(string code, string text);
        public void AddValidationMessage(string code, string text);
        public void AddWarningMessage(string code, string text);
        public void AddErrorMessage(string code, string text);
        public void AddExceptionMessage(string code, string text);
        public void AddFatalErrorMessage(string code, string text);
        public void Merge(IBaseResponse response);
    }
}
