﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedesk.Core.Interfaces
{
    public interface IListResponse:IBaseResponse
    {
        public int TotalItemsOnPage { get; set; }
        public int PageIndex { get; set; }
        public int TotalItems { get; set; }
    }
}
