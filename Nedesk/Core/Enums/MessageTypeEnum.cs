﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedesk.Core.Enums
{
    public enum MessageTypeEnum
    {
        Information = 0,
        Warning = 1,
        Validation = 2,
        Caution = 3,
        Error = 4,
        Exception = 5,
        FatalError = 6
    }
}
