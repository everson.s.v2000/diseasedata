﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Nedesk.Security
{
    public class Encrypter
    {
        private HMACSHA512 _sha512;
        private HMACMD5 _md5;
        public Encrypter()
        {
            _sha512 = new HMACSHA512(UTF8Encoding.UTF8.GetBytes("NEDESK"));
            _md5 = new HMACMD5(UTF8Encoding.UTF8.GetBytes("NEDESK"));
        }

        public string GetMD5String(string value)
        {
            return Md5String(value);
        }
        private string Md5String(string value)
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(value))
            {
                byte[] pkEncoded = UTF8Encoding.UTF8.GetBytes(value);

                byte[] hashedPkEncoded = _md5.ComputeHash(pkEncoded);

                foreach (byte b in hashedPkEncoded)
                {
                    sb.Append(b.ToString("X2"));
                }
            }

            return sb.ToString();
        }

        public string GetEncrpytString(string value, string publicKey = "", string privateKey = "")
        {
            return EncrpytString(value, publicKey, privateKey);
        }

        private string EncrpytString(string value, string publicKey, string privateKey, bool useDataToEncrypt = false, bool doSalt = false)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(this.EncryptProp(value));

            if (!string.IsNullOrWhiteSpace(publicKey))
                sb.Append(this.EncryptProp(publicKey));

            if (!string.IsNullOrWhiteSpace(privateKey))
                sb.Append(this.EncryptProp(privateKey));

            if (useDataToEncrypt)
            {
                string dateToEncode = DateTime.UtcNow.ToString();

                if (!string.IsNullOrWhiteSpace(dateToEncode))
                    sb.Append(this.EncryptProp(dateToEncode));
            }

            if (doSalt)
            {
                string salt = this.EncryptProp(sb.ToString());
                return salt;
            }

            return sb.ToString();

        }

        private string EncryptProp(string value)
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrWhiteSpace(value))
            {
                byte[] pkEncoded = UTF8Encoding.UTF8.GetBytes(value);

                byte[] hashedPkEncoded = _sha512.ComputeHash(pkEncoded);

                foreach (byte b in hashedPkEncoded)
                {
                    sb.Append(b.ToString("X2"));
                }
            }

            return sb.ToString();
        }
    }
}
