﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nedesk.Extensions
{
    public static class StringExtensions
    {
        public static string TargetAsParameter(this string value)
        {
            while(value.Contains('.'))
            {
                value = value.Replace(".", "");
            }

            return value;
        }
    }
}
