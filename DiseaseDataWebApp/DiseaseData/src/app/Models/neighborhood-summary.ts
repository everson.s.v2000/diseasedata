export class NeighborhoodSummary {
    neighborhoodName:string = '';
    numberOfCases:number = 0;
    veryLowInfectionsSeverityCount: number = 0;
    lowInfectionsSeverityCount: number = 0;
    normalInfectionsSeverityCount: number = 0;
    highInfectionsSeverityCount: number = 0;
    veryHighInfectionsSeverityCount: number = 0;
}
