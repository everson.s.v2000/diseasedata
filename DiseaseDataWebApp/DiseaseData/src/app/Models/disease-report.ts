import { Disease } from "./disease";
import { DiseaseReportStatus } from "./disease-report-status";
import { Person } from "./person";

export class DiseaseReport  {
    disease:Disease = new Disease();
    infectedPerson:Person = new Person();
    listStatus:DiseaseReportStatus[] = [];
}
