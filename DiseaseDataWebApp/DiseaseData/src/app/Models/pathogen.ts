import { BaseModel } from "../Nedesk/Core/Models/base-model";
import { LookupType } from "./Types/lookup-type";

export class Pathogen extends BaseModel{
    name:string = '';
    transmissionTypes:LookupType[] = [];
    type:LookupType = new LookupType();
}
