import { BaseModel } from "../Nedesk/Core/Models/base-model";
import { Address } from "./Location/address";
import { LookupType } from "./Types/lookup-type";

export class Person extends BaseModel {
    name:string = '';
    documentType:LookupType = new LookupType();
    uniqueDocument:string = '';
    address:Address = new Address();
}
