import { BaseModel } from "../Nedesk/Core/Models/base-model";
import { LookupType } from "./Types/lookup-type";

export class DiseaseReportStatus extends BaseModel {
    id_DiseaseReport:number = 0;
    infectionSeverityType:LookupType = new LookupType();
}
