import { BaseModel } from "../Nedesk/Core/Models/base-model";
import { NeighborhoodSummary } from "./neighborhood-summary";

export class DiseaseReportSummary extends BaseModel {
    diseaseName:string = '';
    veryHighInfectionsSeverityCount: number = 0;
    highInfectionsSeverityCount: number = 0;
    normalInfectionsSeverityCount: number = 0;
    lowInfectionsSeverityCount: number = 0;
    veryLowInfectionsSeverityCount: number = 0;
    numberOfCases:number = 0;
    deathAverage:number = 0;
    transmissionRate:number = 0;
    neighborhoodSummaries:NeighborhoodSummary[] = [];
}
