import { BaseUser } from "./base-user";
import { Address } from "./Location/address";

export class User extends BaseUser {
    phone:string = '';
    cellphone:string = '';
    cellphone_2:string = '';
    address:Address = new Address();
}

export class CurrentUser {

    private static user:User = new User();

    
    static getUser():User{
        return this.user;
    }

    static setUser(user:User){
        Object.assign(this.user,user);
    }


}

