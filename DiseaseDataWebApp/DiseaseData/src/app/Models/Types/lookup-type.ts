import { BaseModel } from "../../Nedesk/Core/Models/base-model";

export class LookupType extends BaseModel {
    name:string = '';
    description:string = '';
    additionalInfo:string = '';
}
