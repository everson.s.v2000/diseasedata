import { BaseModel } from "../Nedesk/Core/Models/base-model";
import { Pathogen } from "./pathogen";
import { Symptom } from "./symptom";

export class Disease extends BaseModel{
    diseasePathogen:Pathogen = new Pathogen();
    symptoms:Symptom[] = [];
    name:string = '';
    cycleOfLife:string = '';
    treatment:string = '';
    transmissionRate:number = 0;
    deathAverage:number = 0;
}
