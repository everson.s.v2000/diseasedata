import { BaseModel } from "../Nedesk/Core/Models/base-model";

export class BaseUser extends BaseModel {
    name:string = '';
    email:string = '';
    password:string = '';
    
    superAdmin:boolean = false;
}
