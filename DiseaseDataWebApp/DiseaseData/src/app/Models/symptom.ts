import { BaseModel } from "../Nedesk/Core/Models/base-model";

export class Symptom extends BaseModel{
    value:string = '';
    description:string = '';
}
