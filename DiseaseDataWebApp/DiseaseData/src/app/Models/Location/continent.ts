import { State } from "./state";

export class Continent {
    name:string = '';
    countries:State[] = [];
}
