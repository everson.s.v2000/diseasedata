import { BaseModel } from "src/app/Nedesk/Core/Models/base-model";
import { Address } from "./address";

export class Neighborhood extends BaseModel {
    id_City:number = 0;
    name:string = '';
    addresses:Address[] = [];
}
