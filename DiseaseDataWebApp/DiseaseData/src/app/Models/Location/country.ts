import { State } from "./state";

export class Country {
    id_continent:number = 0;
    name:string = '';
    states:State[] = [];
}
