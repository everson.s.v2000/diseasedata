import { BaseModel } from "../../Nedesk/Core/Models/base-model";

export class Address extends BaseModel {
    id_Neighborhood:number = 0;
    number:number = 0;
    street:string = '';
    complement:string = '';
    reference:string = '';
}
