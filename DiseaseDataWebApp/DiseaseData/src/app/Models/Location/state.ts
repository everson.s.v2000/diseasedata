import { City } from "./city";

export class State {
    id_Country:number = 0;
    name:string = '';
    cities:City[] = [];
}
