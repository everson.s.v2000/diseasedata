import { Neighborhood } from "./neighborhood";

export class City {
    id_state:number = 0;
    name:string = '';
    neighborhoods:Neighborhood[] = [];
}
