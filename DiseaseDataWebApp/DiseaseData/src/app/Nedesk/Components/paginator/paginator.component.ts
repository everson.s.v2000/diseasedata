import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { request } from 'http';
import { ListRequest } from '../../Core/Models/list-request';
import { ListResponse } from '../../Core/Models/list-response';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

  @Input()
  public Request: ListRequest = new ListRequest();

  @Input()
  public Response: ListResponse<any> = new ListResponse<any>();

  private RefreshPaginator: boolean = true;
  private oldPageCount: number = 0;
  @Output()
  public Refresh: EventEmitter<boolean> = new EventEmitter();
  public PagesArray: number[] = [];
  constructor() { }

  ngOnInit(): void {
    this.refreshPaginator();


    setInterval(() => {
      this.RefreshPaginator = this.oldPageCount != this.Response.totalPages;
      if (this.RefreshPaginator) {
        this.refreshPaginator();
      }
    })
  }

  emitRefresh(resetPage: boolean = false) {
    if (resetPage) {
      this.Request.pageIndex = 1;
    }
    this.Refresh.emit(true);
  }

  refreshPaginator() {
    if (this.Request.pageIndex == 0) {
      this.Request.pageIndex = 1;
    }
    if (this.Request.limit == 0) {
      this.Request.limit = 10;
    }

    this.PagesArray = [];

    if (this.Response.totalPages <= 0) {
      this.PagesArray.push(1);
    }
    else {
      for (let i = 1; i <= this.Response.totalPages; i++) {
        this.PagesArray.push(i);
      }
    }
    this.oldPageCount = this.PagesArray.length;
  }

  setFirstPage() {
    if (this.Request.pageIndex == 1) return;
    this.Request.pageIndex = 1;
    this.emitRefresh();
  }

  setPreviousPage() {
    if (this.Request.pageIndex <= 1) return;
    if (this.Request.pageIndex > 0) {
      this.Request.pageIndex--;
      this.emitRefresh();
    }
  }

  setNextPage() {
    if (this.Request.pageIndex < this.Response.totalPages) {
      this.Request.pageIndex++;
      this.emitRefresh();
    }
  }

  setLastPage() {
    if (this.Request.pageIndex == this.Response.totalPages) return;
    this.Request.pageIndex = this.Response.totalPages;
    this.emitRefresh();
  }
}
