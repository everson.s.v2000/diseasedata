export enum BasePageBehaviorEnum {
    Insert = 0,
    Update = 1,
    ViewOnly = 2,

    Search = 3,
}


