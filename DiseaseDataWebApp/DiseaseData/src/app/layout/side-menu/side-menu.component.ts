import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Models/user';
import { BaseDDComponent } from 'src/app/Shared/Components/base-ddcomponent/base-dd.component';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent extends BaseDDComponent implements OnInit {

  constructor() {
      super();
   }

  ngOnInit(): void {
  }
 
 
}

