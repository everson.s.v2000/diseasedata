import { Component, OnInit } from '@angular/core';
import { BaseDDComponent } from 'src/app/Shared/Components/base-ddcomponent/base-dd.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends BaseDDComponent implements OnInit {

  constructor() {
      super();
   }

  ngOnInit(): void {
  }

}
