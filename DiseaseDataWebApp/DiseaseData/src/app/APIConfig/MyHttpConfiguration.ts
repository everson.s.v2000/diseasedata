import { Injectable } from '@angular/core';
import { HttpConfiguration } from '../Nedesk/Core/Models/HttpConfiguration';
import { environment } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';


@Injectable()
export class MyHttpConfiguration extends HttpConfiguration {
  constructor() {
    super();

    if (!environment.production) {
      this.enviroment.baseUrl = "https://localhost:5001/api/";
    }
    else {
      this.enviroment.baseUrl = "http://munisaude.eastus.cloudapp.azure.com:4040/api/";
    }
   
  }
}
