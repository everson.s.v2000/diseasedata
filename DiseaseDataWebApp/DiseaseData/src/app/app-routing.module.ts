import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DiseasesComponent } from './Pages/diseases/diseases.component';
import { HomeComponent } from './Pages/home/home.component';
import { ReportsComponent } from './Pages/reports/reports.component';
import { UserPageComponent } from './Pages/user-page/user-page.component';
import { UserUpdatePageComponent } from './Pages/user-update-page/user-update-page.component';

const routes: Routes = [
  {path:"home", component:HomeComponent, },
  {path:"user/register", component:UserPageComponent},
  {path:"user/update", component:UserUpdatePageComponent},
  {path:"reports", component:ReportsComponent},
  {path:"diseases", component:DiseasesComponent},
  {path:"**", redirectTo: 'home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
