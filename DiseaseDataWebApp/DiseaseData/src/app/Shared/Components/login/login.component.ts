import { Component, OnInit } from '@angular/core';
import { BaseDDComponent } from '../base-ddcomponent/base-dd.component';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BaseDDComponent implements OnInit {

    loginVisible: boolean = false;
    constructor() {
        super();
    }

    ngOnInit(): void {
    }


    changeLoginVisiblity() {
        this.loginVisible = !this.loginVisible;
    }

    doLogout() {
        this.loginService.logout().subscribe(response => {
            localStorage.removeItem("Session");
        }, error => {
            this.handleRequestError();
        });

        this.router.navigateByUrl("'/home'");
        
    }
}
