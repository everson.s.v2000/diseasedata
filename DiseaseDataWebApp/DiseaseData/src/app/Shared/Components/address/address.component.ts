import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActionEnum } from 'src/app/Enums/ActionEnum';
import { Address } from 'src/app/Models/Location/address';
import { Neighborhood } from 'src/app/Models/Location/neighborhood';
import { NeighborhoodService } from 'src/app/Services/neighborhood.service';
import { BaseDDComponent } from '../base-ddcomponent/base-dd.component';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent extends BaseDDComponent implements OnInit {
  readonly: boolean = false;
  public Neighborhoods:Neighborhood[] = [];

  @Input()
  public Address: Address = new Address();
  @Input()
  public Action: ActionEnum = ActionEnum.Insert;

  constructor(private neighborhoodService:NeighborhoodService) {
      super();
  }
  ngOnInit(): void {
    this.fetchNeighborhoods();
  }

  async fetchNeighborhoods(){
    try{
      let response = await this.neighborhoodService.fetchAll().toPromise();
      if(!response.inError){
        this.Neighborhoods = <Neighborhood[]> response.responseData;
      }
      this.ShowNotifications(response);
    }
    catch{
      this.handleRequestError();
    }
  }
  

 
  
}
