import { Component, OnInit } from '@angular/core';
import { IBaseResponse } from 'src/app/Nedesk/Core/Models/base-response';
import { ToastPackage, ToastrService } from 'ngx-toastr';
import { MessageTypeEnum } from 'src/app/Nedesk/Core/Models/message-type-enum';
import { AppInjector } from 'src/app/Nedesk/Services/app-injector.service';
import { CurrentUser, User } from 'src/app/Models/user';
import { observable, Observable } from 'rxjs';
import { UserService } from 'src/app/Services/user.service';
import { LoginService } from 'src/app/Services/login.service';
import { BasePageBehaviorEnum } from 'src/app/Enums/base-page-behavior-enum';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
@Component({
    selector: 'app-base-ddcomponent',
    template: '',
    styles: ['']
})
export class BaseDDComponent implements OnInit {

    public BASE_INSERT = BasePageBehaviorEnum.Insert;
    public BASE_UPDATE = BasePageBehaviorEnum.Update;
    public BASE_VIEWONLY = BasePageBehaviorEnum.ViewOnly;
    public BASE_SEARCH = BasePageBehaviorEnum.Search;

    private allowRouteChange: boolean = false;
    private toastrService: ToastrService;
    public userService: UserService;
    public loginService: LoginService;
    public currentUser: User;
    public hasUserLogged: boolean = false;
    public router: Router;
    public ngxService: NgxUiLoaderService
    constructor() {
        const injector = AppInjector.getInjector()

        this.toastrService = injector.get(ToastrService);
        this.currentUser = CurrentUser.getUser();
        this.userService = injector.get(UserService);
        this.loginService = injector.get(LoginService);
        this.router = injector.get(Router);
        this.ngxService = injector.get(NgxUiLoaderService);
        setInterval(() => {
            this.verifyCurrentUser()
        },1000);

    }

    ngOnInit(): void {

    }

    async verifyCurrentUser() {
        await this.validateUser();

        this.hasUserLogged = this.currentUser.id > 0;
        if (!this.hasUserLogged) {
            if(!this.router.url.includes('user/register') && !this.router.url.includes('home')){
                this.router.navigateByUrl("''");
            }
        }
    }
    async validateUser() {
        if (localStorage.getItem("Session")) {
            if (this.currentUser.id <= 0) {
                try {
                    let response = await this.loginService.findBySession().toPromise();
                    if (response.hasResponseData) {
                        CurrentUser.setUser(<User>response.responseData);
                    }
                    else {
                        localStorage.removeItem("Session");
                        window.location.reload();
                        CurrentUser.setUser(new User());
                    }

                }
                catch {
                    this.handleRequestError();
                }

            }
        }
        else {
            if (this.currentUser.id > 0) {
                CurrentUser.setUser(new User());
            }
        }
    }
    handleRequestError() {
        this.toastrService.error("Um erro inesperado ocorreu ao tentar realizar a requisição");
    }
    ShowNotifications(response: IBaseResponse) {
        response.messages.forEach(element => {

            switch (element.messageType) {
                case MessageTypeEnum.Information:
                    this.toastrService.info(element.text);
                    break;
                case MessageTypeEnum.Caution:
                case MessageTypeEnum.Validation:
                case MessageTypeEnum.Warning:
                    this.toastrService.warning(element.text);
                    break;

                case MessageTypeEnum.Error:
                case MessageTypeEnum.Exception:
                case MessageTypeEnum.FatalError:
                    this.toastrService.error(element.text);
                    break;

                default:
                    this.toastrService.show(element.text);
            }
        });
    }
}
