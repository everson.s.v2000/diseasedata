import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { Form, FormControl, FormGroup } from '@angular/forms';
import { ActionEnum } from 'src/app/Enums/ActionEnum';
import { BasePageBehaviorEnum } from 'src/app/Enums/base-page-behavior-enum';
import { LoginTypeEnum } from 'src/app/Enums/LoginTypeEnum';
import { DTOUser } from 'src/app/Models/DTO/dto-user';
import { Address } from 'src/app/Models/Location/address';
import { User } from 'src/app/Models/user';
import { UserService } from 'src/app/Services/user.service';
import { BaseDDComponent } from '../base-ddcomponent/base-dd.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent extends BaseDDComponent implements OnInit {
  
    readonly: boolean = false;
    public BasePageBehaviorText:string = '';

    @Input()
    public BasePageBehavior:BasePageBehaviorEnum = BasePageBehaviorEnum.ViewOnly;

    @Input()
    public User: DTOUser = new DTOUser();
    @Input()
    public Action: ActionEnum = ActionEnum.Insert;
    @Input()
    public ShowActionButtons: boolean = true;
    @Input()
    public ShowAddress: boolean = true;

    @Output()
    public OnSubmit: EventEmitter<boolean> = new EventEmitter<boolean>();
    
    @Output()
    public OnClose: EventEmitter<boolean> = new EventEmitter<boolean>();

    @Output()
    public OnChangeRoute: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output()
    public IsValid: EventEmitter<boolean> = new EventEmitter<boolean>();
    


    constructor(){
        super();
    }

    ngOnInit(): void {
        this.readonly = this.Action === ActionEnum.View;
        if (this.BasePageBehavior == this.BASE_INSERT){
            this.BasePageBehaviorText = 'Registrar'
        }else if (this.BasePageBehavior == this.BASE_UPDATE){
            this.BasePageBehaviorText = 'Atualizar'
        }else if (this.BasePageBehavior == this.BASE_SEARCH){
            this.BasePageBehaviorText = 'Login'
        }else{
            this.BasePageBehaviorText = 'Salvar'
        }
    }

    save(){
        this.OnSubmit.emit(true);
    }

    cancel(){
        this.OnClose.emit(true);
    }
    onChangeRoute(){
        this.OnChangeRoute.emit(true);
    }
}

