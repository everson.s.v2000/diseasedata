import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DTOUser } from 'src/app/Models/DTO/dto-user';
import { CurrentUser, User } from 'src/app/Models/user';
import { BaseResponse } from 'src/app/Nedesk/Core/Models/base-response';
import { LoginService } from 'src/app/Services/login.service';
import { UserService } from 'src/app/Services/user.service';
import { BaseDDComponent } from 'src/app/Shared/Components/base-ddcomponent/base-dd.component';

@Component({
    selector: 'app-login-fixed',
    templateUrl: './login-fixed.component.html',
    styleUrls: ['./login-fixed.component.scss']
})
export class LoginFixedComponent extends BaseDDComponent implements OnInit {

    user: DTOUser = new DTOUser();
    @Input()
    public IsVisible: Boolean = false;
    @Output()
    public IsVisibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
    constructor() {
        super();
     }

    ngOnInit(): void {
    }

    changeVisiblity() {
        this.IsVisible = !this.IsVisible;
        this.IsVisibleChange.emit(false);
    }

    doLogin() {
        this.loginService.login(this.user).subscribe((response) => {
            if(!response.inError){
                localStorage.setItem("Session",<string> response.responseData);
                this.changeVisiblity();

                this.loginService.findBySession().subscribe(userResponse => {
                    if (userResponse.hasResponseData){
                        CurrentUser.setUser(<User> userResponse.responseData);
                    }
                    this.ShowNotifications(userResponse);
                });
            }
            this.ShowNotifications(response);
        }, error => {
            this.handleRequestError();
        });
    }



}
