import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginFixedComponent } from './login-fixed.component';

describe('LoginFixedComponent', () => {
  let component: LoginFixedComponent;
  let fixture: ComponentFixture<LoginFixedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginFixedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFixedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
