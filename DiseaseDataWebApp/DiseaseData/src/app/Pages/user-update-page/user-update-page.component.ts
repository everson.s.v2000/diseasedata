import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DTOUser } from 'src/app/Models/DTO/dto-user';
import { CurrentUser, User } from 'src/app/Models/user';
import { BaseDDComponent } from 'src/app/Shared/Components/base-ddcomponent/base-dd.component';

@Component({
  selector: 'app-user-update-page',
  templateUrl: './user-update-page.component.html',
  styleUrls: ['./user-update-page.component.scss']
})
export class UserUpdatePageComponent  extends BaseDDComponent implements OnInit {
  public user:DTOUser = new DTOUser();

  public isRegisterMode:boolean = false;
  constructor() {
    super();

  
   }

  ngOnInit(): void {
    this.loginService.findBySession().subscribe(response => {
      console.log(response.hasResponseData);
      if (response.hasResponseData){
        this.user = <DTOUser>response.responseData
        console.log(this.user);
      }

      this.ShowNotifications(response);
    })
  }

  doUpdate(){
    this.userService.update(this.user).subscribe(response =>{
      if(!response.inError && !response.hasValidationMessages){
         this.router.navigate(['/']);
         CurrentUser.setUser(this.user);
      }
      this.ShowNotifications(response);
  })
  }
}
