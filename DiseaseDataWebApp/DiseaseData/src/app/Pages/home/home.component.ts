import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BaseDDComponent } from 'src/app/Shared/Components/base-ddcomponent/base-dd.component';
import { HttpService } from '../../Nedesk/Core/Services/http.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseDDComponent implements OnInit  {

  constructor(private httpService:HttpService, private toastService:ToastrService) {
    super();
   }

  ngOnInit(): void {
  }

}
