import { Component, Input, OnInit } from '@angular/core';
import { DiseaseReportSummary } from 'src/app/Models/disease-report-summary';

@Component({
  selector: 'app-report-card',
  templateUrl: './report-card.component.html',
  styleUrls: ['./report-card.component.scss']
})
export class ReportCardComponent implements OnInit {

  public isExpanded:boolean = false;
  @Input()
  public DiseaseReportSummary:DiseaseReportSummary = new DiseaseReportSummary();
  constructor() { }

  ngOnInit(): void {

  }

  changeExpandCollapse(){
    this.isExpanded = !this.isExpanded;
  }
}
