import { Component, OnInit } from '@angular/core';
import { DiseaseReportSummary } from 'src/app/Models/disease-report-summary';
import { FilterOperationType } from 'src/app/Nedesk/Core/Enums/filter-operation-type';
import { Filter } from 'src/app/Nedesk/Core/Models/filter';
import { ListRequest } from 'src/app/Nedesk/Core/Models/list-request';
import { ListResponse } from 'src/app/Nedesk/Core/Models/list-response';
import { DiseaseReportService } from 'src/app/Services/disease-report.service';
import { BaseDDComponent } from 'src/app/Shared/Components/base-ddcomponent/base-dd.component';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent extends BaseDDComponent implements OnInit {
  public Reports: DiseaseReportSummary[] = [];
  public ReportsNeighborhoods: string[] = [];
  public HasReports: boolean = false;
  public Request:ListRequest = new ListRequest();
  public Response:ListResponse<DiseaseReportSummary> = new ListResponse();
  public dataViewMode: string = 'card';
  private filter:Filter = new Filter();
  public filterValue:string = '';
  constructor(private diseaseReportService: DiseaseReportService) {
    super();
  }

  ngOnInit(): void {
   this.fetchAll();
  }

  setFilter(){
    this.Request.filters = [];
    this.Request.filters.push(this.filter);

    this.filter.operationType = FilterOperationType.Like
    this.filter.filterGroup = 0;
    this.filter.target1 = "D.Name"
    this.filter.value1 = `%${this.filterValue}%`
    this.fetchAll();
  }
  fetchAll(){
    
    this.diseaseReportService.fetchAll(this.Request).subscribe(response => {
      if (!response.inError) {
        this.Reports = <DiseaseReportSummary[]>response.responseData;
        this.HasReports = this.Reports && this.Reports.length > 0;

        this.Reports.forEach(element => {
          element.neighborhoodSummaries.forEach(item => {
            if(!this.ReportsNeighborhoods.includes(item.neighborhoodName)){
              this.ReportsNeighborhoods.push(item.neighborhoodName);
            }
          })
        });
      }
      this.ShowNotifications(response);
    }, error => {
      this.handleRequestError();
    })
  }
  setDataViewTable() {
    this.dataViewMode = 'table';
  }
  setDataViewCard() {
    this.dataViewMode = 'card';
  }
}
