import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Disease } from 'src/app/Models/disease';
import { DiseaseReport } from 'src/app/Models/disease-report';
import { DiseaseReportStatus } from 'src/app/Models/disease-report-status';
import { Neighborhood } from 'src/app/Models/Location/neighborhood';
import { Pathogen } from 'src/app/Models/pathogen';
import { FilterOperationType } from 'src/app/Nedesk/Core/Enums/filter-operation-type';
import { BaseResponse } from 'src/app/Nedesk/Core/Models/base-response';
import { Filter } from 'src/app/Nedesk/Core/Models/filter';
import { ListRequest } from 'src/app/Nedesk/Core/Models/list-request';
import { ListResponse } from 'src/app/Nedesk/Core/Models/list-response';
import { DiseaseReportService } from 'src/app/Services/disease-report.service';
import { DiseaseService } from 'src/app/Services/disease.service';
import { NeighborhoodService } from 'src/app/Services/neighborhood.service';
import { BaseDDComponent } from 'src/app/Shared/Components/base-ddcomponent/base-dd.component';

@Component({
  selector: 'app-diseases',
  templateUrl: './diseases.component.html',
  styleUrls: ['./diseases.component.scss']
})
export class DiseasesComponent extends BaseDDComponent implements OnInit {
  public Neighborhoods: Neighborhood[] = [];
  public Response:ListResponse<any> = new ListResponse<any>();
  public Request:ListRequest = new ListRequest();
  public Report: DiseaseReport = new DiseaseReport();
  public CurrentDisease: Disease = new Disease();
  public Diseases: Disease[] = [];
  public IsEditMode: Boolean = false;
  private filter:Filter = new Filter();
  public filterValue:string = '';
  constructor(private modalService: NgbModal,
    private diseaseService: DiseaseService,
    private diseaseReportService: DiseaseReportService,
    private neighborhoodService: NeighborhoodService) {
    super();
  }

  ngOnInit(): void {
    this.fetchNeighborhoods();
    this.fetchDiseases();

  }

  setFilter(){
    this.Request.filters = [];
    this.Request.filters.push(this.filter);

    this.filter.operationType = FilterOperationType.Like
    this.filter.filterGroup = 0;
    this.filter.target1 = "D.Name"
    this.filter.value1 = `%${this.filterValue}%`
    this.fetchDiseases();
  }
  openModal(content: any, disease: Disease = new Disease()) {
    this.IsEditMode = disease.id > 0;
    Object.assign(this.CurrentDisease, disease);
    this.modalService.open(content);
  }

  openReportModal(content: any) {
    this.Report = new DiseaseReport();
    this.Report.listStatus.push(new DiseaseReportStatus());
    this.modalService.open(content);
  }

  save() {
    if (this.IsEditMode) {
      this.diseaseService.update(this.CurrentDisease).subscribe(response => {
        this.ShowNotifications(response);
        this.fetchDiseases();
        if (!response.inError && !response.hasValidationMessages) {
          this.modalService.dismissAll();
        }
      });
    }
    else {
      this.diseaseService.insert(this.CurrentDisease).subscribe(response => {
        this.ShowNotifications(response);
        this.fetchDiseases();
        if (!response.inError && !response.hasValidationMessages) {
          this.modalService.dismissAll();
        }
      });;
    }

  }

  async fetchNeighborhoods() {
    try {
      let response = await this.neighborhoodService.fetchAll().toPromise();
      if (!response.inError) {
        this.Neighborhoods = <Neighborhood[]>response.responseData;
      }
      this.ShowNotifications(response);
    }
    catch {
      this.handleRequestError();
    }
  }
  async fetchDiseases() {
    try {
      let response = await this.diseaseService.fetchAll(this.Request).toPromise();
      if (!response.inError) {
        this.Diseases = <Disease[]>response.responseData;
        this.Response = response;
        console.log(this.Response);
      }
      this.ShowNotifications(response);
    }
    catch {
      this.handleRequestError();
    }
  }

  async saveReport() {
    try {
      let response = await this.diseaseReportService.insert(this.Report).toPromise();
      this.ShowNotifications(response);

      if (!response.inError && !response.hasValidationMessages) {
        this.modalService.dismissAll();
      }

    } catch {
      this.handleRequestError();
    }
  }
}
