import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { LoginTypeEnum } from 'src/app/Enums/LoginTypeEnum';
import { DTOUser } from 'src/app/Models/DTO/dto-user';
import { User } from 'src/app/Models/user';
import { UserService } from 'src/app/Services/user.service';
import { BaseDDComponent } from 'src/app/Shared/Components/base-ddcomponent/base-dd.component';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent extends BaseDDComponent implements OnInit  {

  public user:DTOUser = new DTOUser();

  public isRegisterMode:boolean = false;
  constructor() {
      super();
   }

  ngOnInit(): void {
  }

  doSubmit(){
      this.userService.insert(this.user).subscribe(response =>{
          if(!response.inError && !response.hasValidationMessages){
             this.router.navigate(['/']);
          }
          this.ShowNotifications(response);
      })
  }
}
