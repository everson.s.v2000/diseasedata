import { Injectable } from '@angular/core';
import { User } from '../Models/user';
import { BaseResponse } from '../Nedesk/Core/Models/base-response';
import { HttpService } from '../Nedesk/Core/Services/http.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  private baseUrl:string = 'User'

  constructor(private httpService:HttpService) { }

  insert(user:User){
    return this.httpService.post<BaseResponse<number>>(this.baseUrl,user);
  }
  
  update(user:User){
    return this.httpService.put<BaseResponse<number>>(this.baseUrl,user);
  }
  findByEmail(email:string){
      return this.httpService.get<BaseResponse<User>>(`${this.baseUrl}/FindByEmail?email=${email}`);
  }
  
}
