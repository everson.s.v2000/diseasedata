import { Injectable } from '@angular/core';
import { Disease } from '../Models/disease';
import { BaseResponse } from '../Nedesk/Core/Models/base-response';
import { ListRequest } from '../Nedesk/Core/Models/list-request';
import { ListResponse } from '../Nedesk/Core/Models/list-response';
import { HttpService } from '../Nedesk/Core/Services/http.service';

@Injectable({
  providedIn: 'root'
})
export class DiseaseService {
  private baseUrl: string = 'disease'

  constructor(private httpService: HttpService) { }

  insert(disease:Disease){
    return this.httpService.post<BaseResponse<number>>(this.baseUrl,disease);
  }
  update(disease:Disease){
    return this.httpService.put<BaseResponse<boolean>>(this.baseUrl,disease);
  }
  fetchAll(request:ListRequest){
    return this.httpService.post<ListResponse<Disease>>(`${this.baseUrl}/FetchDiseases`,request);
  }
}
