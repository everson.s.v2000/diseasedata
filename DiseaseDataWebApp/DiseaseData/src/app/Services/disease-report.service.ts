import { Injectable } from '@angular/core';
import { DiseaseReport } from '../Models/disease-report';
import { DiseaseReportSummary } from '../Models/disease-report-summary';
import { BaseResponse } from '../Nedesk/Core/Models/base-response';
import { ListRequest } from '../Nedesk/Core/Models/list-request';
import { ListResponse } from '../Nedesk/Core/Models/list-response';
import { HttpService } from '../Nedesk/Core/Services/http.service';

@Injectable({
  providedIn: 'root'
})
export class DiseaseReportService {

  private baseUrl: string = 'diseaseReport'

  constructor(private httpService: HttpService) { }

  insert(report:DiseaseReport){
    return this.httpService.post<BaseResponse<boolean>>(this.baseUrl,report);
  }

  fetchAll(request:ListRequest){
    return this.httpService.post<ListResponse<DiseaseReportSummary>>(`${this.baseUrl}/FetchSummary`,request);
  }
}
