import { Injectable } from '@angular/core';
import { User } from '../Models/user';
import { BaseResponse } from '../Nedesk/Core/Models/base-response';
import { HttpService } from '../Nedesk/Core/Services/http.service';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    private baseUrl: string = 'Login'

    constructor(private httpService: HttpService) { }

    login(user: User) {
        return this.httpService.post<BaseResponse<string>>(`${this.baseUrl}`,user);
    }
    findBySession(){
        return this.httpService.get<BaseResponse<User>>(`${this.baseUrl}/FindUserBySession`);
    }
    logout(){
        return this.httpService.get<BaseResponse<Boolean>>(`${this.baseUrl}/logout`);
    }
}
