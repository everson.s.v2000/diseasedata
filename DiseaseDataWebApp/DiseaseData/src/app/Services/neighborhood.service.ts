import { Injectable } from '@angular/core';
import { Neighborhood } from '../Models/Location/neighborhood';
import { ListResponse } from '../Nedesk/Core/Models/list-response';
import { HttpService } from '../Nedesk/Core/Services/http.service';

@Injectable({
  providedIn: 'root'
})
export class NeighborhoodService {
  private baseUrl: string = 'neighborhood'

  constructor(private httpService: HttpService) { }

  fetchAll(){
    return this.httpService.get<ListResponse<Neighborhood>>(this.baseUrl);
  }

}
