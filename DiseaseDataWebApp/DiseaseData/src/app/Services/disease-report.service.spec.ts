import { TestBed } from '@angular/core/testing';

import { DiseaseReportService } from './disease-report.service';

describe('DiseaseReportService', () => {
  let service: DiseaseReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DiseaseReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
