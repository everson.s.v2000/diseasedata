import { forwardRef, Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NedeskModule } from './Nedesk/Core/Modules/nedesk.module';
import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { HttpConfiguration } from './Nedesk/Core/Models/HttpConfiguration';
import { MyHttpConfiguration } from './APIConfig/MyHttpConfiguration';
import { HomeComponent } from './Pages/home/home.component';
import { HeaderComponent } from './layout/header/header.component';
import { SideMenuComponent } from './layout/side-menu/side-menu.component';
import { AddressComponent } from './Shared/Components/address/address.component';
import { BaseDDComponent } from './Shared/Components/base-ddcomponent/base-dd.component';
import { AppInjector } from './Nedesk/Services/app-injector.service';
import { UserComponent } from './Shared/Components/userComponent/user.component';
import { User } from './Models/user';
import { LoginComponent } from './Shared/Components/login/login.component';
import { UserPageComponent } from './Pages/user-page/user-page.component';
import { LoginFixedComponent } from './Shared/Components/login-fixed/login-fixed.component';
import { UserUpdatePageComponent } from './Pages/user-update-page/user-update-page.component';
import { ReportsComponent } from './Pages/reports/reports.component';
import { ReportCardComponent } from './Pages/reports/Components/report-card/report-card.component';
import { DiseasesComponent } from './Pages/diseases/diseases.component';
import { NgxMaskModule } from 'ngx-mask';
import { NgxUiLoaderHttpModule, NgxUiLoaderModule, NgxUiLoaderRouterModule } from 'ngx-ui-loader';
import { PaginatorComponent } from './Nedesk/Components/paginator/paginator.component';
@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        HeaderComponent,
        SideMenuComponent,
        AddressComponent,
        UserComponent,
        BaseDDComponent,
        LoginComponent,
        UserPageComponent,
        LoginFixedComponent,
        UserUpdatePageComponent,
        ReportsComponent,
        ReportCardComponent,
        DiseasesComponent,
        PaginatorComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule, // required animations module
        ToastrModule.forRoot(), // ToastrModule added
        AppRoutingModule,
        HttpClientModule,
        NgxUiLoaderModule,
        NgxUiLoaderRouterModule,
        NgxUiLoaderHttpModule,
        NgbModule,
        NgxMaskModule.forRoot(),
        NedeskModule,

    ],
    providers: [
        { provide: HttpConfiguration, useClass: MyHttpConfiguration },
        { provide: AppInjector, useClass:AppInjector},
    ],
    bootstrap: [AppComponent]
})

export class AppModule {
    constructor(injector:Injector){
        AppInjector.setInjector(injector);
    }
}

